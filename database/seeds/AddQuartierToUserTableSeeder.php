<?php

use Illuminate\Database\Seeder;
use App\Quartier;
use App\User;

class AddQuartierToUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quartier = Quartier::all()->first()->id;
        $users = User::all();

        foreach ($users as $user)
        {
            $user->quartier_id = $quartier;
            $user->save();
        }
    }
}
