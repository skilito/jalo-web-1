<?php

use Illuminate\Database\Seeder;
use App\Categorie;

class AddCategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categorie::create([
            "libelle" => "Cosmetic",
            "icone" => "images/icones/cosmetics.png",
            "visible" => true
        ]);

        Categorie::create([
            "libelle" => "Legume",
            "icone" => "",
            "visible" => true
        ]);

        Categorie::create([
            "libelle" => "Mode",
            "icone" => "",
            "visible" => true
        ]);

        Categorie::create([
            "libelle" => "Cuisine",
            "icone" => "",
            "visible" => true
        ]);

        Categorie::create([
            "libelle" => "Produit de grande surface",
            "icone" => "",
            "visible" => true
        ]);
    }
}
