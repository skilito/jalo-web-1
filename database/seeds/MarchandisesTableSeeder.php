<?php

use Illuminate\Database\Seeder;
use App\Produit;
use  App\Marchandise;
use App\User;

class MarchandisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

        $vendeur1 = \App\Role::where('role', 'fournisseur')->first()->id;
        $vendeur2 = \App\Role::where('role', 'fournisseurFMCG')->first()->id;


        for ($i = 0; $i < 50; $i++)
        {

            Marchandise::create([
                'vendeur_id' => $faker->randomElement([User::where('role_id', $vendeur1)->first()->id, User::where('role_id', $vendeur2)->first()->id]),
                'produit_id' => $faker->numberBetween(Produit::all()->first()->id, Produit::all()->last()->id),
                'prix' => $faker->numberBetween($min = 100, $max = 10000),
                'quantite_dispo' => $faker->numberBetween($min = 1, $max = 900),
                'date_ajout'=> $faker->dateTime,
            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
