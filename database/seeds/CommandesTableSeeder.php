<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Commande;
use App\Role;
class CommandesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();
        $client = Role::where('role', 'client')->first()->id;

        for ($i = 0; $i < 100; $i++)
        {
            $letter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
            $number =  $faker->unique()->randomNumber(5);
            Commande::create([
                'date_commande' => $faker->dateTime,
                'reference' => $number.strtoupper($letter),
                'client_id' => $faker->numberBetween(User::where('role_id', $client)->first()->id, User::where('role_id', $client)->get()->last()->id)
            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }


}
