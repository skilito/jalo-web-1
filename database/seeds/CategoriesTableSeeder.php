<?php

use Illuminate\Database\Seeder;
use App\Categorie;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

        for ($i=0; $i<6; $i++)
        {

            Categorie::create([
                "libelle" => $faker->word
            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
