<?php

use Illuminate\Database\Seeder;
use App\Quartier;

class QuartiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quatiers = ["MARISTES","PARCELLE ASSAINIES", "CITE DJILY MBAYE","OUAKAM","GRAND MBAO", "NORD FOIRE",
            "PETIT MBAO", "PIKINE", "GUEDIAWAYE", "GRAND YOFF", "CAMBERENE", "HLM", "CASTOR", "LIBERTE 4",
            "GRAND DAKAR", "AMITIE", "MERMOZ", "FANN RESIDENCE", "YOFF APIX 2", "CITE KEUR GORGUI","FASS PAILLOTE",
            "GIBRALTAR", "SACREE COEUR", "SICAP BAOBAB", "HLM GRAND YOFF", "DIEUPPEUL DERKLE",
            "LIBERTE 6 EXTENSION", "SCAT URBAM", "ALMADIE", "LIBERTE 6", "PLATEAU", "GUEULE TAPEE", "FANN HOCK",
            "ZONE A ZONE B", "POINT E"];

        foreach ($quatiers as $quatier){
            Quartier::create([
                'nom' => $quatier
            ]);
        }
    }
}
