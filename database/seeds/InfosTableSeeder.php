<?php

use Illuminate\Database\Seeder;
use App\Info;

class InfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $infos = ["internet","journal", "par le biais d'un ami"];

        foreach ($infos as $info){
            Info::create([
                'label' => $info
            ]);
        }
    }
}
