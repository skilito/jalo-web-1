<?php

use Illuminate\Database\Seeder;
use App\Catalogue;

class UpdatePricePromoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catalogues = Catalogue::all();
        foreach ($catalogues as $catalogue)
        {
            if($catalogue->promo_prix == null)
            {
                $catalogue->promo_prix = 0;
                $catalogue->saveOrFail();
            }
        }
    }
}
