<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Vente;

class VentesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = $this->getFaker();
        $client_id = \App\Role::where('role', 'client')->first()->id;
        $type = array('grossiste', 'detail');

        for ($i = 0; $i < 50; $i++)
        {
            $letter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 3);
            $number =  $faker->unique()->randomNumber(5);

            Vente::create([
                'client_id' => $faker->randomElement([User::where('role_id',$client_id)->first()->id, User::where('role_id',$client_id)->get()->last()->id]),
                'date_vente' => $faker->dateTime,
                'quantite' => $faker->numberBetween($min = 1, $max = 10),
                'reference' => $number.strtoupper($letter),
                'type' => array_rand($type)

            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
