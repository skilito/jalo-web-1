<?php

use Illuminate\Database\Seeder;
use App\CatalogueCommande;

class AddEtatIdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();

      foreach ( CatalogueCommande::all() as $value) {
      	 $value->etat_id = $faker->numberBetween(CatalogueCommande::all()->first()->id, CatalogueCommande::all()->last()->id);
      }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
