<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoutiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boutiques', function (Blueprint $table) {
            $table->unsignedInteger('boutiquier_id')->nullable();
            $table->unsignedInteger('commercial_id')->nullable();
            $table->foreign('boutiquier_id')->references('id')->on('users')->onDelete('cascade')->onupdate('cascade');
            $table->foreign('commercial_id')->references('id')->on('users')->onDelete('cascade')->onupdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boutiques');
    }
}
