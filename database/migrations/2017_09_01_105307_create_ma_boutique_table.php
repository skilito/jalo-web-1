<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaBoutiqueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ma_boutique', function ($table) {
            $table->increments('id');
            $table->unsignedInteger('boutiquier_id')->nullable();
            $table->unsignedInteger('client_id')->nullable();
            $table->foreign('boutiquier_id')->references('id')->on('users')->onDelete('cascade')->onupdate('cascade');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade')->onupdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ma_boutique');
    }
}
