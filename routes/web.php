<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('register', 'Auth\RegisterController@index')->name('register');
Route::post('register', 'Auth\RegisterController@create');

Route::get('login', 'Auth\LoginController@index')->name('login');
Route::post('login', 'Auth\LoginController@store')->name('post_login');


Route::get('logout', 'Auth\LoginController@destroy');

Route::get('client/catalogues', 'CatalogueController@index')->name('client_catalogues');

Route::post('client/add_panier', 'PanierController@create');

Route::post('client/panier', 'PanierController@panier');

Route::post('panier/edit', 'PanierController@index');

Route::post('panier/edit', 'PanierController@index');

Route::get('client/commandes', 'CommandeController@index');

Route::get('autocomplete', 'ProduitController@indexautocomplete')->name('autocomplete');

Route::get('commercial/catalogues', 'CommercialController@index')->name('dashbord-commercial');

Route::get('commercial/commandes', 'CommercialController@getOrdersByCommercail')->name('commercial-commande');

Route::get('commercial/commande', 'CommercialController@show')->name('commande');

Route::get('commercial/clients', 'CommercialController@getClients')->name('commercial-clients');

Route::get('commercial/fournisseurs', 'CommercialController@getFournisseurs')->name('commercial-fournisseurs');

Route::get('commercial/{type}/{user_id}/show', 'CommercialController@showUser');
Route::get('boutiques', 'HomeController@getShoppers');
Route::get('catalogues', 'HomeController@getcatalogues');




//ajax request

Route::post('client/panier/remove', 'PanierController@remove');

Route::post('client/panier/list', 'PanierController@getList');

Route::post('client/panier', 'PanierController@update');

Route::post('client/product/price', 'CatalogueController@getPrice');

Route::post('client/commande', 'CommandeController@store');



// fin ajax request

Route::group(['namespace' => 'Api'], function ()
{
    Route::group(['prefix' => 'api/v1'], function ()
    {
        Route::post('auth/register', 'AuthController@register');
        Route::post('auth/login', 'AuthController@login');
        Route::get('quartiers', 'QuartierController@getAllQuartier');
        Route::get('categories', 'CategorieController@index');
        Route::get('roles', 'UserController@getAllRoles');

        Route::get('boutiquier/{id}/clients', 'ClientController@index');

        Route::group(['middleware' => 'jwt.auth'], function () {
            Route::get('user', 'AuthController@getAuthUser');  // detail_ user
            Route::get('catalogues/category/{categorie_id}', 'CatalogueController@index');  // liste catalogue
            Route::get('users/fournisseurfmcg','UserController@getUsers');  // liste users fournisseurFMCG
            Route::get('category/products','ProduitController@getListProductToCategory');
            Route::get('product/{id}','ProduitController@show');
            Route::get('catalogue/{id}','CatalogueController@show');
            Route::get('catalogue/{id}/numberViews','CatalogueController@incrementNumberView');

            Route::post('user', 'UserController@update');
            Route::get('provider/products', 'ProduitController@getListProductsUser');
            Route::get('catalogues/promo', 'CatalogueController@getListCataloguePromo');
            Route::get('stocks', 'StockController@index');
            Route::post('commande', 'CommandeController@create');
            Route::get('client/commandes', 'CommandeController@index');
            Route::post('vente', 'VenteController@create');
            Route::get('boutiquiers', 'UserController@getBoutiquiersByQuartier');
            Route::post('add/boutiquier/favoris', 'MaBoutiqueController@addBoutiquierFavoris');
            Route::get('boutiquier/{boutiquier}/produits', 'UserController@getProductByShoper');
            Route::get('commande/{commande_id}/annulee', 'CommandeController@annuleeCommande');
            Route::get('client/boutiquier/favoris', 'MaBoutiqueController@getBoutiquierFavoris');
            Route::post('stock', 'StockController@create');
            Route::patch('stock', 'StockController@update');
            Route::post('boutiquier/client/add', 'ClientController@createCostumer');
            Route::patch('boutiquier/client', 'ClientController@update');
            Route::get('products', 'ProduitController@getAllProduct');
            Route::get('boutiquier/{boutiquier_id}/commandes', 'CommandeController@getallOrderByShopper');
            Route::get('boutiquier/{boutiquier_id}/ventes', 'VenteController@getVenteByShopper');
            Route::post('user/update/image', 'UpdateImageController@updateImage');
            Route::patch('commande', 'CommandeController@update');
            Route::get('commercial/commandes', 'CommandeController@getCommandesBycommercial');   # à modifier pour le 1
            Route::get('fournisseurs', 'UserController@getFournisseur');
            Route::get('commercial/clients', 'ClientController@getClientsByCommercial');
            Route::get('catalogues', 'CatalogueController@getAllCatalogues');
            Route::get('notification', 'NotificationController@create');
            Route::get('search', 'ProduitController@search');
            Route::get('client/{telephone}', 'ClientController@getClient');
            Route::get('boutiquier/ventes', 'VenteController@getSoldersByShopper');
            Route::get('commercial/ventes', 'VenteController@getSoldersBySaler');

            # notification
            Route::post('fcm', 'fcmController@create');
            Route::put('fcm', 'fcmController@update');
            Route::get('commandes/{commande_id}/call', 'CommandeController@call');
        });
    });
});


Route::group(['namespace' => 'admin'], function ()
{
    Route::get('/admin/category/new' , 'categories@create');
    Route::post('/admin/categorie/save' , 'categories@store');

    Route::get('admin/dashboard', 'DashboardController@index');
    Route::get('admin/produits', 'AdminProductController@index');
    Route::post('admin/commercial' , 'AdminUserController@create_commercial');
    Route::get('admin/commandes' , 'AdminConmmandeController@index');
    Route::get('admin/clients' , 'AdminUserController@getClients')->name('clients');
    Route::get('admin/fournisseurs' , 'AdminUserController@getFournisseurs');
    Route::get('admin/fournisseur/{fournisseur_id}/produits' , 'AdminProductController@getProducts');
    Route::get('admin/{client}/{user_id}' , 'AdminUserController@show');
    Route::get('admin/{type}/edit/{user_id}' , 'AdminUserController@editUser')->name('editUser');
    Route::post('admin/user' , 'AdminUserController@update')->name('user');
    Route::get('admin/fournisseurs' , 'AdminUserController@getAllFournisseurs')->name('fournisseurs');
    Route::get('admin/commerciaux' , 'AdminUserController@getAllCommerciaux')->name('commerciaux');
    Route::get('admin/boutiques' , 'AdminBoutiqueController@index')->name('boutiques');
    Route::get('commande' , 'AdminConmmandeController@changeStatus');
    Route::get('admin/ventes', 'AdminVenteController@getVentes');
    Route::get('admin/boutiquiers', 'AdminUserController@getAllboutiquiers');
    Route::get('admin/client/show/{client_id}', 'AdminClientController@show');
    Route::get('admin/client/{client_id}/{edit}', 'AdminClientController@edit');
    Route::post('admin/client-boutiquier/update', 'AdminClientController@update');
    Route::get('{type}/{user_id}/delete', 'AdminUserController@destroy');
    Route::get('admin/categories', 'categories@index');
    Route::get('produit/{produit_id}', 'AdminProductController@show');

    // ajax
    Route::post('admin/boutiquier' , 'DashboardController@createBoutiquier');
    Route::post('admin/commercial/edit' , 'DashboardController@editCommercial');
    Route::post('admin/commercial/update' , 'DashboardController@updateCommercial');
    Route::post('admin/boutiquier/edit' , 'DashboardController@editBoutiquier');
    Route::post('admin/boutiquier/update' , 'DashboardController@updateBoutiquier');
    Route::post('admin/client' , 'DashboardController@createClient');
    Route::post('admin/client/edit' , 'DashboardController@editClient');
    Route::post('admin/client/update' , 'DashboardController@updateClient');
    Route::post('admin/produit' , 'AdminProductController@create');
    Route::get('/produit/edit/{catalogue_id}' , 'AdminProductController@edit');
//  Route::get('admin/commande' , 'AdminConmmandeController@show');
    Route::post('admin/boutique' , 'AdminBoutiqueController@create');
    Route::get('commande/show/{commande_id}' , 'AdminConmmandeController@showCommande');
    Route::post('/product/update' , 'AdminProductController@update');
    Route::post('admin/product/delete' , 'AdminProductController@destroy');
    Route::post('/admin/administateur' , 'DashboardController@addAdmin');
    Route::post('/admin/change-password' , 'AdminClientController@changePassword');
    Route::get('/admin/categorie/{categorie_id}/delete' , 'categories@delete');
    Route::get('/admin/categorie/{categorie_id}/show' , 'categories@show');
    Route::get('/admin/categorie/{categorie_id}/edit' , 'categories@edit');
    Route::post('/admin/categorie' , 'categories@update');

    Route::get('commande/{commande_id}/calling', 'AdminConmmandeController@calling');
//  Route::get('/admin/product/edit' , 'AdminProductController@edit');


});





