@extends('layouts.app')
@section('content')
    <div class="preview">

        <div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle="example-menu"></button>
            <div class="title-bar-title">Menu</div>
        </div>
        <header class="header-home">
            <div class="row">
                <div class="top-bar" id="example-menu">
                    <a href="/"><img src="./images/logo-yellow.jpeg" alt="Logo JALO" class="logo-home"></a>
                    <div class="top-bar-right">
                        <ul class="menu">
                            {{--<li><a href="/login " class="underline-hover" >Admin</a></li>
                            <li><a href="#" class="underline-hover" >Boutiquier</a></li>
                            <li><a href="#" class="underline-hover">Client</a></li>

                            <li><a href="" class="underline-hover">|</a></li>


                            <li><a href="/login" class="underline-hover" >Se connecter</a></li>

                            <li><a href="/register" class="hollow button secondary">S'inscrire</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div class="jumbotron">
            <div class="row align-middle">
                <div class="small-12 medium-8 large-5">
                    <h2 class="jumbotron-title">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                    <div class="jumbotron-btn">
                        <a href="/boutiques" class="btn yellow waves-effect waves-black">Voir nos boutiques</a>
                        <a href="/catalogues" class="btn white waves-effect waves-black">Voir nos produits </a>
                    </div>
                </div>
            </div>
            <img class="preview-mobile" src="./images/nexus5x.png" alt="">
        </div>

        <section class="section section-services">

            <div class="row">
                <div class="small-12 medium-6 large-4">
                    <div class="service">
                        <img src="images/services/commande.svg" alt="">
                        <h4 class="service-title">Commandez chez votre boutiquier</h4>
                        <p class="service-sumary">Votre boutiquier dispose maintenant d’une tablette JALÔ qui vous donne accès à plus de 2.000 produits à des tarifs négociés auprès de notre réseau de fournisseurs. Si nécessaire, notre service de télémarketing vous appelle instantanément.</p>
                    </div>
                </div>
                <div class="small-12 medium-6 large-4">
                    <div class="service">
                        <img src="images/services/devices.svg" alt="" class="mb">
                        <h4 class="service-title">Payez instantanément, en cash ou en paiement mobile</h4>
                        <p class="service-sumary">Vous achetez en ligne, mais vous payez hors ligne, par cash, ou via Orange Money. Vous n’avez pas tout le montant requis ? Pas d’inquiétude, contactez votre boutiquier du quartier pour des facilités de paiement.</p>
                    </div>
                </div>
                <div class="small-12 medium-6 large-4">
                    <div class="service">
                        <img src="images/services/commercial.svg" alt="" class="mb10">
                        <h4 class="service-title">Faites-vous livrer chez votre boutiquier</h4>
                        <p class="service-sumary">Vous avez confiance en votre boutiquier du quartier, nous aussi. Nous assurons la livraison de vos dans un délai maximum de 48H, à la boutique de quartier. Vous êtes alertés dès que la livraison est effective.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-white">

            <div class="row">
                <div class="small-12 medium-12 large-6 large-offset-3">
                    <h4 class="section-title">Vidéo</h4>
                    <div class="responsive-embed">
                        <video width="100%" height="320" controls="controls">
                            <source src="videos/demo-jalo.mp4" type="video/mp4" />
                        </video>
                    </div>
                </div>
            </div>
        </section>

        <div class="featured-testimonials-section">
            <div class="featured-testimonials row">
                <div class="small-12 medium-12 large-12 columns">
                    <div class="testimonial">
                        <div>
                            <h2>LES BONNES AFFAIRES DU MOIS</h2>
                            <br>
                            <a href="/catalogues" class="btn yellow waves-effect waves-black">Voir nos catalogues</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section">
            <div class="row">
                <h4 class="section-title p-10">Les derniers produits</h4>
            </div>

            <div class="row">

                @foreach($catalogues as $catalogue)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">
                            <img src="{{url('/')}}/{{$catalogue->photo}}" alt="">
                            <div class="card-section">
                                <a href="/produit/{{$catalogue->id}}"><h4 class="card-title">{{ ucfirst($catalogue->produit) }}</h4></a>
                                {{--<small class="card-sub-title">{{ ucfirst($catalogue->prenom) }} {{$catalogue->nom}}</small>--}}
                                <div class="card-wrap-price">
                                    @if($catalogue->promo_prix != 0)
                                        <strong class="card-price-old">{{$catalogue->prix}}<span>F cfa</span></strong>
                                        <strong class="card-price">{{$catalogue->promo_prix}}<span>F cfa</span></strong>
                                    @else
                                        <strong class="card-price">{{$catalogue->prix}}<span>F cfa</span></strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <p>&nbsp;</p>
            {{--<div class="row align-center">--}}
                {{--<div class="see-more">--}}
                    {{--<a href="" class="btn yellow waves-effect waves-black">Voir nos boutiques</a>--}}
                {{--</div>--}}
            {{--</div>--}}

        </section>

        <footer class="footer">
            <div class="row">

                <div class="small-12 medium-6 large-4">
                    <div class="footer-brand">
                        <img src="images/logo-yellow.jpeg" alt="">
                        <p>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines.</p>
                        {{--<a href="https://play.google.com/store?hl=en" target="_blank" class="mobile"><img src="./images/android-download.svg" alt=""></a>--}}
                    </div>
                </div>

                <div class="small-12 medium-6 large-3 large-offset-2">
                    {{--<h4 class="section-title">A propos de JALO</h4>--}}
                    {{--<ul class="menu vertical">--}}
                        {{--<li><a href="">Historique</a></li>--}}
                        {{--<li><a href="">Devenir Agent Jalo</a></li>--}}
                        {{--<li><a href="">Devenir Boutiquier</a></li>--}}
                        {{--<li><a href="">Recrutement</a></li>--}}
                    {{--</ul>--}}
                </div>
                <div class="small-12 medium-6 large-3">
                    <h4 class="section-title">Nous contacter</h4>
                    <address>
                        <strong>85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal</strong>
                        <br>
                        <span>+221 33 824 81 95  <i>&nbsp;</i> | <i>&nbsp;</i> 77 225 94 32 <i>&nbsp;</i> | <i>&nbsp;</i> 78 384 01 04 </span>
                        <br>
                        <span>info@jaloshops.com</span>
                    </address>
                    <ul class="menu horizontal">
                        <li><a href="https://www.facebook.com/jalo.jalo.1297943" target="_blank"><img src="images/social-network/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
@endsection