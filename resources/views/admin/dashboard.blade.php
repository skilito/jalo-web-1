@extends('layouts.admin.master-admin')
@section('title')
    JALÔ
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-pink card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{ $nbre_boutiquier }}</strong>
                            <a href="/admin/boutiquiers" class="waves-effect waves-yellow"><i class="tiny material-icons">person</i> <span>Boutiquiers</span></a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-blue card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{$commade_en_cours}}</strong>
                            <a href="/admin/commandes" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes en cours</span></a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-blue-dark card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{$nbr_client}}</strong>
                            <a href="/admin/clients" class="waves-effect waves-yellow"><i class="tiny material-icons">group</i> <span>Clients</span></a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-blue-dark card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{ $nbr_fournisseur }}</strong>
                            <a href="/admin/fournisseurs" class="waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-pink card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{$produits}}</strong>
                            <a href="/admin/produits" class="waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Produits</span></a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="small-12 medium-6 large-4 p-10 wrapper-card-dash">
                <a href="">
                    <div class="card card-color card-blue card-center">
                        <div class="card-section card-section-dash">
                            <strong>{{$ventes}}</strong>
                            <a href="/admin/ventes" class="waves-effect waves-yellow"><i class="tiny material-icons">attach_money</i> <span>Ventes</span></a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-6 large-4 p-10">
                <div class="card">
                    <header class="card-header"><strong>Commerciaux {{$nbre_commerciaux }}</strong><a href="#new-seller" class="modal-trigger dash-add-new">+</a></header>
                    <div class="card-body">
                        <table class="unstriped">
                            @foreach($commerciaux as $commercial)
                                <tr>
                                    <td>
                                        <img src="@if($commercial->avatar != null) {{url('/')}}/{{$commercial->avatar}} @else /images/jumbotron-background.png @endif " alt="" class="avatar">
                                    </td>
                                    <td><span>{{ ucfirst($commercial->prenom)}} {{$commercial->nom}}</span></td>
                                    <td>
                                        <a href="#show-item-seller" class="modal-trigger" onclick="showModalEditCommercial({{ $commercial->id }})"><i class="material-icons">remove_red_eye</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-6 large-4 p-10">
                <div class="card">
                    <header class="card-header"><strong>Boutiquier {{$nbre_boutiquier}}</strong><a href="#new-shopper" class="modal-trigger dash-add-new">+</a></header>
                    <div class="card-body">
                        <table class="unstriped">
                            @foreach($array_boutiquier as $boutiquier)
                                <tr>
                                    <td><img src="@if($boutiquier->avatar != null) {{url('/')}}/{{$boutiquier->avatar}} @else /images/jumbotron-background.png @endif" alt="" class="avatar"></td>
                                    <td><span>{{ ucfirst($boutiquier->prenom) }} {{ ucfirst($boutiquier->nom) }}</span></td>
                                    <td>
                                        <a href="#show-shopper" class="modal-trigger" onclick="showModalEditBoutiquier({{ $boutiquier->id }})"><i class="material-icons">remove_red_eye</i></a>
                                        <a href="/boutiquier/{{$boutiquier->id}}/delete" class="modal-trigger"><i class="material-icons" onclick="return confirm('Etes vous sur de voulloir supprimer ce boutiquier?');">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-6 large-4 p-10">
                <div class="card">
                    <header class="card-header"><strong>Clients {{$nbr_client}}</strong>
                    </header>
                    <div class="card-body">
                        <table class="unstriped">
                            @foreach( $clients as $client)
                                <tr>
                                    <td><span>{{utf8_decode($client->prenom)}} {{$client->nom}}</span></td>
                                    <td>
                                        <a href="#edit-client" class="modal-trigger"><i class="material-icons" onclick="showModalEditClient({{ $client->id }})">remove_red_eye</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Structure -->
        <div id="add-admin" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Nouveau Administrateur</h4>
                </header>
                <p>&nbsp;</p>
                <form action="" id="saveAdmin">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="prenomAdmin" type="text" class="validate" name="prenomAdmin" pattern="[\w\s()!,.-]{3,45}">
                                <label for="prenomAdmin"  data-error="..." >Prénom *</label>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="nomAdmin" type="text" class="validate"  name="nomAdmin" pattern="[\w\s()!,.-]{3,45}">
                                <label for="nomAdmin"  data-error="..." >Nom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="tel_admin" type="text" class="validate" name="tel_admin" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                                <label for="tel_admin"  data-error="..." >Téléphone *</label>
                            </div>
                            <div class="errorAdmin">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="PasseAdmin" type="password" class="validate" name="PasseAdmin">
                                <label for="PasseAdmin"  data-error="..." >Mot de Passe *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="EmailAdmin" type="email" class="validate" name="EmailAdmin" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                                <label for="EmailAdmin"  data-error="..." >Adresse Email *</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!"  class="modal-action  waves-effect waves-black btn-flat grey button" form="saveAdmin" type="submit">CRÉER</button>
            </div>
        </div>    {{-- Create new boutiquier --}}
        <div id="new-shopper" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Nouveau Boutiquier</h4>
                </header>
                <p>&nbsp;</p>
                <form action="" id="new-boutiquier">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="nom_boutiquier"  data-error="..." >Prénom *</label>
                                <input id="nom_boutiquier" type="text" class="validate" name = 'nom_boutiquier'  required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="prenom_boutiquier"  data-error="..." >Nom *</label>
                                <input id="prenom_boutiquier" type="text" class="validate" name="prenom_boutiquier"  required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="tel_boutiquier"  data-error="..." >Téléphone *</label>
                                <input id="tel_boutiquier_name" type="text" class="validate" name="tel_boutiquier" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                            <div class="errorBou">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="email_boutiquier"  data-error="..." >Adresse Email </label>
                                <input id="email_boutiquier" type="email" class="validate" name="email_boutiquier">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="password_boutiquier"  data-error="..." >Mot de Passe *</label>
                                <input id="password_boutiquier" type="password" class="validate" name="password" required>
                            </div>
                            <div class="errorPassword">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="confirpassword_boutiquier"  data-error="..." >Confirmer mot de passe *</label>
                                <input id="confirpassword_boutiquier" type="password" class="validate" name="confirpassword" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="adresse_boutiquier"  data-error="..." >Adresse </label>
                                <input id="adresse_boutiquier" type="text" class="validate" name="adresse_boutiquier" pattern="[\w\s()!,.-]{3,160}" >
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <select name="quartier" required>
                                <option value="" disabled selected>Sélectioner votre quartier</option>
                                @foreach($quartiers as $quartier)
                                    <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2" style="margin-top: 20px">
                            <select name="commercial_id" required>
                                <option value="" disabled selected>Sélectioner le commercial</option>
                                @foreach($commerciaux as $commercial)
                                    <option value="{{$commercial->id}}">{{$commercial->prenom}} {{$commercial->nom}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2" style="margin-top: 10px">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo profil</span>
                                    <input name="new_photo" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" class="modal-actionwaves-effect waves-black btn-flat grey button" type="submit" form="new-boutiquier">CRÉER</button>

            </div>
        </div>

        <div id="new-seller" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Nouveau Commercial</h4>
                </header>
                <p>&nbsp;</p>
                <form action="" id="save-commercial">
                    <div class="row">

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="new_prenom"  data-error="..." >Prénom *</label>
                                <input id="new_prenom" type="text" class="validate" name="new_prenom" pattern="[\w\s()!,.-]{3,45}"  required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="new_nom"  data-error="..." >Nom *</label>
                                <input id="new_nom" type="text" class="validate" name="new_nom" pattern="[\w\s()!,.-]{3,45}" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="new_tel"  data-error="..." >Téléphone *</label>
                                <input name="new_tel" id="new_tel" type="tel" class="validate" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                            <div class="error">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="new_email"  data-error="..." >Email *</label>
                                <input name="new_email" id="new_email" type="email" class="validate">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="password_commercial"  data-error="..." >Mot de Passe *</label>
                                <input id="password_commercial" type="password" class="validate" name="password" required>
                            </div>
                            <div class="errorPassword">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="confirpassword_commercial"  data-error="..." >Confirmer mot de passe *</label>
                                <input id="confirpassword_commercial" type="password" class="validate" name="confirpassword" required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <label for="new_adresse"  data-error="..." >Adresse </label>
                                <input name="new_adresse" id="new_adresse" type="text" class="validate" pattern="[\w\s()!,.-]{3,45}">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <select  required name="quartier">
                                <option value="" disabled selected>Sélectioner votre quartier</option>
                                @foreach($quartiers as $quartier)
                                    <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                @endforeach
                                {{--<label for="fournisseur"  data-error="..." >Quartier *</label>--}}
                            </select>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo profil</span>
                                    <input name="new_photo" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>

                <button type="submit" form="save-commercial"  class="modal-action  waves-effect waves-black btn-flat grey button" onclick="">CRÉER</button>
            </div>
        </div>

        <div id="show-item-seller" class="modal modal-fixed-footer">   {{-- edit commerciaux--}}
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Edit Commercial</h4>
                </header>
                <p>&nbsp;</p>
                <form action=""  id="edit-commercial">
                    <div class="row">

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="prenom_commercial"  data-error="..." class="active-l">Prénom *</label>
                            <div class="input-field">
                                <input id="prenom_commercial" pattern="[\w\s()!,.-]{3,45}" type="text" class="validate" name = 'prenom_commercial' required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="nom_commercial"  data-error="..."  class="active">Nom *</label>
                            <div class="input-field">
                                <input id="nom_commercial"  pattern="[\w\s()!,.-]{3,45}" type="text" class="validate" name = 'nom_commercial' required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="tel_commercial"  data-error="..."  class="active">Téléphone *</label>
                            <div class="input-field">
                                <input id="tel_commercial"  type="tel" class="validate" name = 'tel_commercial' pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="email_commercial"  data-error="..."  class="active">Email *</label>
                            <div class="input-field">
                                <input id="email_commercial" type="email" class="validate" name = 'email_commercial' pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="adresse_commercial"  data-error="..."  class="active-l">Adresse </label>
                            <div class="input-field">
                                <input id="adresse_commercial" minlength="3" type="text" class="validate" name = 'adresse_commercial' pattern="[\w\s()!,.-]{3,160}">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo profil</span>
                                    <input name="new_photo" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!"  class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" type="submit" form="edit-commercial" class="modal-action waves-effect waves-black btn-flat grey button" >CRÉER</button>
            </div>
        </div>

        <div id="new-client" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Nouveau client</h4>
                <form action="" id="save-client">
                    <input type="hidden" name="role" value="client">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="prenom_client" type="text" class="validate" name="prenom_client" pattern="A-Za-z]{}" required>
                                <label for="prenom_client"  data-error="ce prenom n'existe pas" >Prénom *</label>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="nom_client" type="text" class="validate" name="nom_client" required>
                                <label for="nom_client"  data-error="ce nom n'existe pas" pattern="A-Za-z]{}" >Nom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="tel_client" type="tel" class="validate" name="tel_client" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                                <label for="tel_client"  data-error="numéro telephone invalide" >Téléphone *</label>
                            </div>
                            <div class="errorCli">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="email_client" type="email" class="validate" name="email_client">
                                <label for="email_client"  data-error="adresse email invalide" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">email </label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="adresse_client" type="text" class="validate" name="adresse_client">
                                <label for="adresse_client"  data-error="cet adresse n'existe pas" pattern="A-Za-z]{}" >Adresse *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <select name="quartier" required>
                                    <option value="" disabled selected>Sélectioner votre quartier</option>
                                    @foreach($quartiers as $quartier)
                                        <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                    @endforeach
                                    @if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                {{--<a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button" onclick="Materialize.toast('client créé', 4000, 'rounded')">CRÉER CLIENT</a>--}}
                <button href="#!" class="modal-actionwaves-effect waves-black btn-flat grey button" type="submit" form="save-client">CRÉER</button>
            </div>
        </div>

        <div id="show-shopper" class="modal modal-fixed-footer">   {{-- edit boutiquier --}}
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Edit Boutiquier</h4>
                </header>
                <p>&nbsp;</p>
                <form action=""  id="edit-boutiquier">
                    <div class="row">

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editNom_bou"  data-error="..." >Prénom *</label>
                            <div class="input-field">
                                <input id="editNom_bou" pattern="[\w\s()!,.-]{3,45}" type="text" class="validate" name = 'editNom_bou' required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editPrenom_bou"  data-error="..." >Nom *</label>
                            <div class="input-field">
                                <input id="editPrenom_bou"  pattern="[\w\s()!,.-]{3,45}" type="text" class="validate" name = 'editPrenom_bou' required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editTel_bou"  data-error="..." >Téléphone *</label>
                            <div class="input-field">
                                <input id="editTel_bou"  type="tel" class="validate" name = 'editTel_bou' pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editEmail_bou"  data-error="..." >Email *</label>
                            <div class="input-field">
                                <input id="editEmail_bou" type="email" class="validate" name = 'editEmail_bou' pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editAdresse_bou"  data-error="..." >Adresse </label>
                            <div class="input-field">
                                <input id="editAdresse_bou" minlength="3" type="text" class="validate" name = 'editAdresse_bou'>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo profil</span>
                                    <input name="new_photo" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!"  class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" type="submit" form="edit-boutiquier" class="modal-action waves-effect waves-black btn-flat grey button" >CRÉER</button>
            </div>
        </div>

        <div id="edit-client" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Edit client</h4>
                <form action="" id="saveEditClient">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editNom_client"  data-error="ce nom invalite" pattern="[\w\s()!,.-]{3,45}" >Prénom *</label>
                            <div class="input-field">
                                <input id="editNom_client" type="text" class="validate" name="editNom_client" required>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editPrenom_client"  data-error="ce prenom invalide" pattern="[\w\s()!,.-]{3,45}" >Nom *</label>
                            <div class="input-field">
                                <input id="editPrenom_client" type="text" class="validate" name="editPrenom_clien" required>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editTel_client"  data-error="numéro email invalide" >Téléphone *</label>
                            <div class="input-field">
                                <input id="editTel_client" type="tel" class="validate" name="editTel_client" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" required>
                            </div>
                        </div>
                        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
                        {{--<label for="email_client"  data-error="adresse email invalide" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">email </label>--}}
                        {{--<div class="input-field">--}}
                        {{--<input id="editEmail_client" type="email" class="validate" name="editEmail_client">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editAdresse_client"  data-error="cet adresse n'est pas valide" pattern="[\w\s()!,.-]{3,160}" >Adresse *</label>
                            <div class="input-field">
                                <input id="editAdresse_client" type="text" class="validate" name="editAdresse_client">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <a href="#!"  class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" type="submit" form="saveEditClient" class="modal-action waves-effect waves-black btn-flat grey button" >CRÉER</button>
            </div>
        </div>
    </main>

@section('script')
    <script src="../js/vendors/jquery.min.js"></script>
    <script src="../js/vendors/foundation.min.js"></script>
    <script src="../js/vendors/materialize.min.js"></script>
    <script src="../js/app.js"></script>
@endsection

<script>

    $(document).ready(function(){

        $('#save-commercial').on('submit', function(event){
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'commercial',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    if(data=='error')
                    {
                        $('.error').append("<label style='color: red'>Ce numero existe deja</label>")
                    }
                    else if(data=='errorpassword')
                    {
                        $('.errorPassword').append("<label style='color: red'>les mots de passe ne correspondent pas</label>")
                    }
                    $('#new-seller').modal('close')
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })

    function showModalEditCommercial(commercial_id)
    {
        $(document).ready(function()
        {
            $.post('commercial/edit',
                {
                    commercial_id: commercial_id,
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $('#edit-commercial .row  .input-field #prenom_commercial').val(data.prenom);
                    $('#edit-commercial .row  .input-field #nom_commercial').val(data.nom);
                    $('#edit-commercial .row  .input-field #tel_commercial').val(data.phone1);
                    $('#edit-commercial .row  .input-field #email_commercial').val(data.email);
                    $('#edit-commercial .row  .input-field #adresse_commercial').val(data.adresse);
                });


            $('#edit-commercial').on('submit', function(event){
                event.preventDefault();

                var formData = new FormData($(this)[0]);

                formData.append('commercial_id', commercial_id);

                $.ajax({
                    url: 'commercial/update',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#show-item-seller').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })

        });
    }

    $(document).ready(function(){

        $('#new-boutiquier').on('submit', function(event){
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: '/admin/boutiquier',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    if(data=='error')
                    {
                        $('.errorBou').append("<label style='color: red'>Ce numero existe deja</label>")
                    }
                    else if(data=='errorpassword')
                    {
                        $('.errorPassword').append("<label style='color: red'>les mots de passe ne correspondent pas</label>")
                    }
                    else {

                        $('#new-shopper').modal('close')
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })

    function showModalEditBoutiquier(boutiquier_id)
    {
        $(document).ready(function()
        {
            $.post('boutiquier/edit',
                {
                    boutiquier_id: boutiquier_id,
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $('#edit-boutiquier .row  .input-field #editPrenom_bou').val(data.prenom);
                    $('#edit-boutiquier .row  .input-field #editNom_bou').val(data.nom);
                    $('#edit-boutiquier .row  .input-field #editTel_bou').val(data.phone1);
                    $('#edit-boutiquier .row  .input-field #editEmail_bou').val(data.email);
                    $('#edit-boutiquier .row  .input-field #editAdresse_bou').val(data.adresse);
                });


            $('#edit-boutiquier').on('submit', function(event){
                // alert('ok');
                var formData = new FormData($(this)[0]);

                formData.append('boutiquier_id', boutiquier_id);

                $.ajax({
                    url: 'boutiquier/update',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#show-shopper').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })

        });
    }


    $(document).ready(function(){

        $('#save-client').on('submit', function(event)
        {

            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'client',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {

                    if(data=='error')
                    {
                        $('.errorCli').append("<label style='color: red'>Ce numero existe deja</label>")
                    }
                    else {
                        $('#new-client').modal('close')
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })

    function showModalEditClient(client_id)
    {
        $(document).ready(function()
        {
            $.post('client/edit',
                {
                    client_id: client_id,
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $('#saveEditClient .row  .input-field #editPrenom_client').val(data.prenom);
                    $('#saveEditClient .row  .input-field #editNom_client').val(data.nom);
                    $('#saveEditClient .row  .input-field #editTel_client').val(data.phone1);
                    $('#saveEditClient .row  .input-field #editEmail_client').val(data.email);
                    $('#saveEditClient .row  .input-field #editAdresse_client').val(data.adresse);
                    //$('#saveEditClient .row  #selectId select').val(data.quartier_id);
                });


            $('#saveEditClient').on('submit', function(event){
                event.preventDefault();

                var formData = new FormData($(this)[0]);

                formData.append('client_id', client_id);

                $.ajax({
                    url: 'client/update',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#edit-client').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })

        });
    }

    $(document).ready(function(){

        $('#saveAdmin').on('submit', function(event){
            //alert('ok');
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'administateur',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    if(data=='error')
                    {
                        $('.errorAdmin').append("<label style='color: red'>Ce numero existe deja</label>")
                    }
                    else
                    {
                        console.log(data)
                        $('#add-admin').modal('close')
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })

</script>

@endsection