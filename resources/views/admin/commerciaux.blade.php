@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>

        {{--<form action="clients" method="get">--}}
            {{--<div class="row">--}}
                {{--<div class="small-12 medium-6 large-4 p-10">--}}
                    {{--<label for="">--}}
                        {{--Quartier--}}
                        {{--<select name="quartier">--}}
                            {{--<option value="" disabled selected>Sélectioner votre quartier</option>--}}
                            {{--@foreach($quartiers as $quartier)--}}
                                {{--<option value="{{$quartier->nom}}">{{$quartier->nom}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</label>--}}
                {{--</div>--}}
                {{--<div class="small-12 medium-6 large-3 p-10">--}}
                    {{--<label for="">--}}
                        {{--Date début--}}
                        {{--<input type="date" name="dateDebut">--}}
                    {{--</label>--}}
                {{--</div>--}}
                {{--<div class="small-12 medium-6 large-3 p-10">--}}
                    {{--<label for="">--}}
                        {{--Date Fin--}}
                        {{--<input type="date" name="dateFin">--}}
                    {{--</label>--}}
                {{--</div>--}}
                {{--<div class="small-12 medium-6 large-2 p-10 filter">--}}
                    {{--<button class="button expanded" style="margin-bottom: 0;">Filter</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</form>--}}
        {{--<p>&nbsp;</p>--}}

        <!-- Liste des commandes -->
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Prénom & Nom</th>
                        <th>Quartier</th>
                        <th>Adresse</th>
                        <th>Téléphone</th>
                        <th>Date Creation</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{ ucfirst($client->prenom) }} {{ ucfirst($client->nom) }}</td>

                            <td>{{ ucfirst($client->quartier) }}</td>
                            <td> {{ ucfirst($client->adresse) }}</td>
                            <td> {{ $client->phone1 }}</td>
                            <td> {{ $client->date_inscription }}</td>
                            <td>
                                @if (Request::is('admin/clients'))
                                    <a href="client/show/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="client/{{$client->id}}/edit" class="modal-trigger"><i class="material-icons">edit</i></a>
                                    <a href="/client/{{$client->id}}/delete" class="modal-trigger"><i class="material-icons" onclick="return confirm('Etes vous sur de voulloir supprimer ce user?');">delete</i></a>
                                @elseif(Request::is('admin/fournisseurs'))
                                    <a href="fournisseur/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="fournisseur/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                    <a href="/fournisseur/{{$client->id}}/delete" class="modal-trigger"><i class="material-icons" onclick="return confirm('Etes vous sur de voulloir supprimer ce fournisseur?');">delete</i></a>
                                @else
                                    <a href="commercial/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="commercial/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                    <a href="/commercial/{{$client->id}}/delete" class="modal-trigger"><i class="material-icons" onclick="return confirm('Etes vous sur de voulloir supprimer ce commercial?');">delete</i></a>
                                @endif
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $clients->appends(request()->query())->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection

