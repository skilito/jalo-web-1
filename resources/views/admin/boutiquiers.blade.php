@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>
        <!-- Liste des commandes -->
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Prénom & Nom</th>
                        <th>Quartier</th>
                        <th>Adresse</th>
                        <th>Téléphone</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($boutiquiers as $client)
                        <tr>
                            <td>{{ ucfirst($client->prenom) }} {{ ucfirst($client->nom) }}</td>

                            <td>{{ ucfirst($client->quartier) }}</td>
                            <td> {{ ucfirst($client->adresse) }}</td>
                            <td> {{ $client->phone1 }}</td>
                            <td>
                                @if (Request::is('admin/clients'))
                                    <a href="client/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="client/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                @elseif(Request::is('admin/fournisseurs'))
                                    <a href="fournisseur/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="fournisseur/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                @elseif(Request::is('admin/boutiquiers'))
                                    <a href="boutiquier/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="boutiquier/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                @else
                                    <a href="commercial/{{$client->id}}" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                    <a href="commercial/edit/{{$client->id}}" class="modal-trigger"><i class="material-icons">edit</i></a>
                                @endif
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $boutiquiers->appends(request()->query())->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection