
@extends('layouts.admin.master-admin')

@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>
        <div class="row">
            <li> <a href="produits" class="button dropdown-button">Toutes les catégories</a> </li>
            <li> <a href="produits" class="button dropdown-button " data-activates="dropdown1">catégorie</a> </li>
            <ul id="dropdown1" class="dropdown-content">
                @foreach($categories as $categorie)
                    <li><a href="produits?categorie={{$categorie->id}}">{{$categorie->libelle}}</a></li>
                @endforeach
            </ul>
        </div>
        <!-- Liste des produits -->
        <div class="row">
            @foreach($catalogues as $catalogue)
                <div class="small-12 medium-6 large-3 p-10">
                    <div class="card card-center">
                        <img src="{{url('/')}}/{{$catalogue->photo}}" alt="">
                        <div class="card-section">
                            <a href="/produit/{{$catalogue->id}}"><h4 class="card-title">{{ ucfirst($catalogue->produit) }}</h4></a>
                            <small class="card-sub-title">{{ ucfirst($catalogue->prenom) }} {{$catalogue->nom}}</small>
                            <div class="card-wrap-price">
                                @if($catalogue->promo_prix != 0)
                                    <strong class="card-price-old">{{$catalogue->prix}}<span>F cfa</span></strong>
                                    <strong class="card-price">{{$catalogue->promo_prix}}<span>F cfa</span></strong>
                                @else
                                    <span class="card-price">{{$catalogue->prix}}<span>F cfa</span></span>
                                @endif
                                <i class="material-icons">remove_red_eye</i><strong class="card-price">{{$catalogue->numberViews}}</strong>
                            </div>
                            {{--<a href="/admin/produit/edit" class="btn-floating btn-tiny waves-effect waves-yellow yellow btn-edit modal-trigger" onclick="showModalEditProduct({{$catalogue->id}})">--}}
                            <a href="/produit/edit/{{$catalogue->id}}" class="btn-floating btn-tiny waves-effect waves-yellow yellow btn-edit modal-trigger">
                                <i class="material-icons yellow">edit</i>
                            </a>
                            <form class="delete-product">
                                <input type="hidden" name="catalogue_id" value="{{$catalogue->id}}" class="catalogue_id">
                                {{ csrf_field() }}
                                <button class="btn-floating btn-tiny waves-effect waves-yellow yellow btn-delete"
                                        onclick="return confirm('Etes vous sur de voulloir supprimer ce produit?');">
                                    <i class="material-icons red">delete</i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                    {{ $catalogues->appends(request()->query())->links() }}
                </ul>
            </div>
        </div>
        {{--edit product--}}
        {{--<div id="edit-product" class="modal modal-fixed-footer">--}}
        {{--<div class="modal-content">--}}
        {{--<header class="header-modal">--}}
        {{--<h4>Edit Produit</h4>--}}
        {{--</header>--}}
        {{--<p>&nbsp;</p>--}}
        {{--<form action="" id="saveEditProduct">--}}
        {{--<div class="row">--}}
        {{--<div class="fournisseur_id">--}}

        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
        {{--<label for="editNom"  data-error="..." >Nom Produit *</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="editNom" type="text" class="validate"  name="editNom">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
        {{--<label for="editDescription"  data-error="..." >Description Produit *</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="editDescription" type="text" class="validate"  name="editDescription">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
        {{--<label for="editPrice"  data-error="..." >Prix *</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="editPrice" type="number" class="validate"  name="editPrice">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--{{ csrf_field() }}--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
        {{--<label for="editFournisseur"  data-error="..." >Fournisseur *</label>--}}
        {{--<div class="input-field-custom">--}}
        {{--<select id="fournisseur-edit" name="editFournisseur">--}}
        {{--<option value="0"></option>--}}
        {{--@foreach($fournisseurs as $fournisseur)--}}
        {{--<option value="{{$fournisseur->id}}">{{ucfirst($fournisseur->prenom)}} {{ucfirst($fournisseur->nom)}} </option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2" style="margin-top: 20px">--}}
        {{--<label for="EditPrixPromo"  data-error="..." >Prix promo</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="EditPrixPromo" type="number" class="validate"  name="EditPrixPromo">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<p>&nbsp;</p>--}}
        {{--<div class="small-12 medium-12 large-8 large-offset-2">--}}
        {{--<div class="row no-margin">--}}
        {{--<div class="small-12 medium-12 large-5">--}}
        {{--<label for="EditPourcentageJalo"  data-error="..." >Pourcentage JALO</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="EditPourcentageJalo" type="number" class="validate"  name="EditPourcentageJalo">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-5 large-offset-2">--}}
        {{--<label for="EditPourcentageBoutiquier"  data-error="..." >Pourcentage Boutiquier</label>--}}
        {{--<div class="input-field">--}}
        {{--<input id="EditPourcentageBoutiquier" type="number" class="validate"  name="EditPourcentageBoutiquier">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="small-12 medium-12 large-6 large-offset-2">--}}
        {{--<div class="file-field input-field">--}}
        {{--<div class="btn lh13 bgc-yellow">--}}
        {{--<span>Photo Produit</span>--}}
        {{--<input type="file" name="new_photo">--}}
        {{--</div>--}}
        {{--<div class="file-path-wrapper">--}}
        {{--<input class="file-path validate" >--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--<div class="modal-footer">--}}
        {{--<a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>--}}
        {{--<button href="#!" class="modal-action waves-effect waves-black btn-flat grey button" form="saveEditProduct" type="submit">MODIFIER</button>--}}
        {{--</div>--}}
        {{--</div>--}}
    </main>

@section('script')
    <script src="../js/vendors/jquery.min.js"></script>
    <script src="../js/vendors/foundation.min.js"></script>
    <script src="../js/vendors/materialize.min.js"></script>
    <script src="../js/app.js"></script>
@endsection

<script>
    {{--function showModalEditProduct(catalogue_id)--}}
    {{--{--}}
    {{--$(document).ready(function()--}}
    {{--{--}}
    {{--$.post('produit/edit',--}}
    {{--{--}}
    {{--catalogue_id: catalogue_id,--}}
    {{--"_token": $("input[name=_token]").val()--}}
    {{--}, function(data)--}}
    {{--{--}}
    {{--$('#saveEditProduct .row  .input-field #editNom').val(data[0].nomProduit);--}}
    {{--$('#saveEditProduct .row  .input-field #editPrice').val(data[0].prix);--}}
    {{--$('#saveEditProduct .row  .input-field #EditPourcentageJalo').val(data[0].pourcentage_jalo);--}}
    {{--$('#saveEditProduct .row  .input-field #EditPourcentageBoutiquier').val(data[0].pourcentage_boutiquier)--}}
    {{--$('#saveEditProduct .row  .input-field #EditPrixPromo').val(data[0].promo_prix)--}}
    {{--$('#saveEditProduct .row  .input-field #editDescription').val(data[0].description)--}}
    {{--$("#fournisseur-edit option[value= \"0\"]").text(data[0].fournisseur_prenom.replace(/\b\w/g, function(l){--}}
    {{--return l.toUpperCase() }) + ' '+ data[0].fournisseur_nom.replace(/\b\w/g, function(l){ return l.toUpperCase()--}}
    {{--}));--}}
    {{--});--}}


    {{--$('#saveEditProduct').on('submit', function(event){--}}
    {{--event.preventDefault();--}}

    {{--var formData = new FormData($(this)[0]);--}}

    {{--console.log(formData);--}}

    {{--formData.append('catalogue_id', catalogue_id);--}}

    {{--$.ajax({--}}
    {{--url: 'product/update',--}}
    {{--type: 'POST',--}}
    {{--data: formData,--}}
    {{--async: false,--}}
    {{--success: function (data) {--}}
    {{--$('#edit-product').modal('close')--}}
    {{--location.reload();--}}
    {{--},--}}
    {{--cache: false,--}}
    {{--contentType: false,--}}
    {{--processData: false--}}
    {{--});--}}

    {{--return false;--}}
    {{--})--}}

    {{--$(".dropdown-button").dropdown();--}}

    {{--});--}}
    {{--}--}}

    $('.delete-product').on('submit', function(event){
        event.preventDefault();

        var formData = new FormData($(this)[0]);

        $(document).ready(function()
        {
            $.ajax({
                url: 'product/delete',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });

        });
    })
</script>
@endsection