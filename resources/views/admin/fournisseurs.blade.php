@extends('layouts.admin.master-admin')
@section('title')
<<<<<<< HEAD
    JALÔ | Fournisseurs
=======
    JALÔ - Tableau de bord
>>>>>>> dev
@endsection

@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')

<main class="main">
    <p>&nbsp;</p>
    <!-- Liste des fournisseurs -->
    <div class="row">
        @foreach($fournisseurs as $fournisseur)
            <div class="small-12 mendium-6 large-3 p-10">
                <div class="card card-center">
                    <img src="https://source.unsplash.com/category/nature" alt="">
                    <div class="card-section">
                        <img src="{{ $fournisseur->image }}" alt="" class="avatar-provider">
                        <h3 class="card-title avatar-img-top">{{ ucfirst( $fournisseur->prenom) }} {{ ucfirst( $fournisseur->nom) }}</h3>
                        <a href="fournisseur/{{$fournisseur->id}}/produits" class="add-bucket-btn waves-effect waves-black"> Voir ses produits </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <!-- Modal Structure -->
    <div id="add-product" class="modal modal-fixed-footer">
        <div class="modal-content">
            <header class="header-modal">
                <h4>Nouveeau Produit</h4>
            </header>
            <p>&nbsp;</p>
            <form action="">
                <div class="row">

                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name"  data-error="..." >Nom Produit *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="number" class="validate">
                            <label for="last_name"  data-error="..." >Prix *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <select>
                                <option value="" disabled selected>Fournisseur</option>
                                <option value="1">Fournisseur A</option>
                                <option value="2">Fournisseur B</option>
                                <option value="3">Fournisseur C</option>
                            </select>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="row no-margin">
                            <div class="small-12 medium-12 large-5">
                                <div class="input-field">
                                    <input id="last_name" type="number" class="validate">
                                    <label for="last_name"  data-error="..." >Pourcentage JALO</label>
                                </div>
                            </div>
                            <div class="small-12 medium-12 large-5 large-offset-2">
                                <div class="input-field">
                                    <input id="last_name" type="number" class="validate">
                                    <label for="last_name"  data-error="..." >Pourcentage Boutiquier</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-6 large-offset-2">
                        <div class="file-field input-field">
                            <div class="btn lh13 bgc-yellow">
                                <span>Photo Produit</span>
                                <input type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('Produit crée', 4000, 'rounded')">CRÉER</a>
        </div>
    </div>
    <div id="edit-product" class="modal modal-fixed-footer">
        <div class="modal-content">
            <header class="header-modal">
                <h4>Nouveeau Produit</h4>
            </header>
            <p>&nbsp;</p>
            <form action="">
                <div class="row">

                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate" value="Lait">
                            <label for="last_name"  data-error="..." >Nom Produit *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="number" class="validate" value="600">
                            <label for="last_name"  data-error="..." >Prix *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <select>
                                <option value="" disabled>Fournisseur</option>
                                <option value="1" selected >Fournisseur A</option>
                                <option value="2">Fournisseur B</option>
                                <option value="3">Fournisseur C</option>
                            </select>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="row no-margin">
                            <div class="small-12 medium-12 large-5">
                                <div class="input-field">
                                    <input id="last_name" type="number" class="validate" value="3">
                                    <label for="last_name"  data-error="..." >Pourcentage JALO</label>
                                </div>
                            </div>
                            <div class="small-12 medium-12 large-5 large-offset-2">
                                <div class="input-field">
                                    <input id="last_name" type="number" class="validate" value="4">
                                    <label for="last_name"  data-error="..." >Pourcentage Boutiquier</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-6 large-offset-2">
                        <div class="file-field input-field">
                            <div class="btn lh13 bgc-yellow">
                                <span>Photo Produit</span>
                                <input type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('Produit modifié', 4000, 'rounded')">MODIFIÉ</a>
        </div>
    </div>
    <div id="add-provider" class="modal modal-fixed-footer">
        <div class="modal-content">
            <header class="header-modal">
                <h4>Nouveeau Fournisseur</h4>
            </header>
            <p>&nbsp;</p>
            <form action="">
                <div class="row">

                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name"  data-error="..." >Nom fournisseur *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="tel" class="validate">
                            <label for="last_name"  data-error="..." >Téléphone *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="email" class="validate">
                            <label for="last_name"  data-error="..." >Email *</label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="input-field">
                            <input id="last_name" type="text" class="validate">
                            <label for="last_name"  data-error="..." >Presonne à contacter </label>
                        </div>
                    </div>
                    <div class="small-12 medium-12 large-8 large-offset-2">
                        <div class="row no-margin">
                            <div class="small-12 medium-12 large-4">
                                <div class="file-field input-field">
                                    <div class="btn lh13 bgc-yellow">
                                        <span>Logo</span>
                                        <input type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="small-12 medium-12 large-8">
                                <div class="file-field input-field">
                                    <div class="btn lh13 bgc-yellow">
                                        <span>Photo couverture</span>
                                        <input type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('Fournisseur crée', 4000, 'rounded')">CRÉER</a>
        </div>
    </div>

</main>
@endsection

@section('script')
    <script src="../js/vendors/jquery.min.js"></script>
    <script src="../js/vendors/foundation.min.js"></script>
    <script src="../js/vendors/materialize.min.js"></script>
    <script src="../js/app.js"></script>
@endsection
