@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../../../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Edit {{ $type }} </h4>
                </header>
                <p>&nbsp;</p>
                <form action="{{route('user')}}" id="editUser" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="type" value="{{$type}}">
                    <input type="hidden" name="user_id" value="{{$user->id}}" />
                    <div class="row">
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="{{$user->prenom}}" name="editUser_prenom">
                                <label for="last_name"  data-error="..." >Prénom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" name="editUser_nom" value="{{$user->nom}}">
                                <label for="last_name"  data-error="..." >Nom *</label>
                            </div>
                        </div>

                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="tel" class="validate" name="editUser_tel" value="{{$user->phone1}}" pattern="[0-9]{2}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}">
                                <label for="last_name"  data-error="..." >Téléphone *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="email" class="validate" name="editUser_email" value="{{$user->email}}">
                                <label for="last_name"  data-error="..." >Email *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" name="editUser_adresse" value="{{$user->adresse}}">
                                <label for="last_name"  data-error="..." >Adresse </label>
                            </div>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
                {{--<hr>--}}
                <div class="form-footer">
                    <button type="submit" class="modal-action modal-close waves-effect waves-black btn-flat success button" form="editUser">ENREGISTRER</button>
                    @if($type == 'Fournisseur')
                        <a href="{{route('fournisseurs')}}" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                    @elseif($type == 'Client')
                        <a href="{{route('clients')}}" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                    @elseif($type == 'Commercial')
                        <a href="{{route('commerciaux')}}" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                    @endif
                </div>
            </div>
        </div>

    </main>

@section('script')
    <script src="../../../js/vendors/jquery.min.js"></script>
    <script src="../../../js/vendors/foundation.min.js"></script>
    <script src="../../../js/vendors/materialize.min.js"></script>
    <script src="../../../js/app.js"></script>
@endsection

@endsection
