@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>
        <!-- Liste des commandes -->
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        {{--<th>Produits</th>--}}
                        <th>Reference</th>
                        <th>CLient</th>
                        <th>Téléphone Client</th>
                        <th>Date Commande</th>
                        <th>Boutiquier</th>
                        <th>Téléphone Boutiquier</th>
                        <th>Commercial</th>
                        <th>Téléphone Commercial</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($ventes != [])
                        @foreach($ventes as $commande)
                            <tr>
                                <td>{{$commande->reference}}</td>
                                <td>{{ ucfirst($commande->prenomClient) }} {{ ucfirst($commande->nomClient)}}</td>
                                <td>{{ $commande->phoneClient}}</td>
                                <td>{{ $commande->date_commande }}</td>
                                <td>{{ ucfirst($commande->prenomBou) }} {{ ucfirst($commande->nomBoutiquier)}}</td>
                                <td>{{ $commande->telBou}}</td>
                                <td>{{ ucfirst($commande->prenomCom)}} {{ucfirst($commande->nomCom)}}</td>
                                <td>{{ $commande->telCom }}</td>
                                <td>{{ $commande->etat }}</td>
                                {{--<td><a href="#show-item-commande" class="modal-trigger"><i class="material-icons" onclick="showProduct({{ $commande->id}})">remove_red_eye</i></a></td>--}}
                                <td><a href="/commande/show/{{$commande->id}}"><i class="material-icons">remove_red_eye</i></a></td>
                            </tr>

                            <!-- Modal Structure -->
                            {{--<div id="show-item-commande" class="modal modal-fixed-footer" data-item="item-{{ $commande->id }}">--}}

                            {{--</div>--}}
                        @endforeach
                    @else
                        <h1>Pas de commande</h1>
                    @endif
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $ventes->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        {{--<p><button class="button" data-open="item-commande">Click me for a modal</button></p>--}}

        <div id="add-product" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Nouveeau Produit</h4>
                </header>
                <p>&nbsp;</p>
                <form action="">
                    <div class="row">

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate">
                                <label for="last_name"  data-error="..." >Nom Produit *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="last_name" type="number" class="validate">
                                <label for="last_name"  data-error="..." >Prix *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <select>
                                    <option value="" disabled selected>Fournisseur</option>
                                    <option value="1">Fournisseur A</option>
                                    <option value="2">Fournisseur B</option>
                                    <option value="3">Fournisseur C</option>
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="row no-margin">
                                <div class="small-12 medium-12 large-5">
                                    <div class="input-field">
                                        <input id="last_name" type="number" class="validate">
                                        <label for="last_name"  data-error="..." >Pourcentage JALO</label>
                                    </div>
                                </div>
                                <div class="small-12 medium-12 large-5 large-offset-2">
                                    <div class="input-field">
                                        <input id="last_name" type="number" class="validate">
                                        <label for="last_name"  data-error="..." >Pourcentage Boutiquier</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo Produit</span>
                                    <input type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('Produit crée', 4000, 'rounded')">CRÉER</a>
            </div>
        </div>
        <div id="edit-product" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Nouveeau Produit</h4>
                </header>
                <p>&nbsp;</p>
                <form action="">
                    <div class="row">

                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="Lait">
                                <label for="last_name"  data-error="..." >Nom Produit *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <input id="last_name" type="number" class="validate" value="600">
                                <label for="last_name"  data-error="..." >Prix *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="input-field">
                                <select>
                                    <option value="" disabled>Fournisseur</option>
                                    <option value="1" selected >Fournisseur A</option>
                                    <option value="2">Fournisseur B</option>
                                    <option value="3">Fournisseur C</option>
                                </select>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="row no-margin">
                                <div class="small-12 medium-12 large-5">
                                    <div class="input-field">
                                        <input id="last_name" type="number" class="validate" value="3">
                                        <label for="last_name"  data-error="..." >Pourcentage JALO</label>
                                    </div>
                                </div>
                                <div class="small-12 medium-12 large-5 large-offset-2">
                                    <div class="input-field">
                                        <input id="last_name" type="number" class="validate" value="4">
                                        <label for="last_name"  data-error="..." >Pourcentage Boutiquier</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo Produit</span>
                                    <input type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('Produit modifié', 4000, 'rounded')">MODIFIÉ</a>
            </div>
        </div>

    </main>

@section('script')
    <script src="../js/vendors/jquery.min.js"></script>
    <script src="../js/vendors/foundation.min.js"></script>
    <script src="../js/vendors/materialize.min.js"></script>
    <script src="../js/app.js"></script>
@endsection

<script>

    $(document).ready(function()
    {
        $(document).on('closeme.zf.reveal', function() {
            var modal = $(this);
        })

    })


    function showProduct(commande_id)
    {
        //console.log(commande_id);
        $(document).ready(function()
        {

            jQuery.ajax({
                url:'commande',
                type: 'GET',
                data: {
                    commande_id: commande_id,
                },
                success: function(data)
                {

                    // console.log(data['commande']['date_commande']);
                    $('.client').append(`
                        <header class="header-modal">
                            <h4>Confirmation</h4>
                            <date>${data['commande']['date_commande']}</date>
                        </header>
                        <p>&nbsp;</p>
                        <h4>${data['commande']['prenom']} ${data['commande']['nom']}</h4>
                        <strong>${data['commande']['phone1']}</strong>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>`);

                    $.each(data['produits'], function( key, value) {
                        //console.log(value);
                        $('.produit').append(`
                                   <div class="row">
                                    <div class="small-12 medium-12 large-6"><strong style= "text-transform: capitalize">${value['libelle']}</strong></div>
                                    <div class="small-12 medium-12 large-2"><strong>${value['quantite']}</strong></div>
                                    <div class="small-12 medium-12 large-2"><strong>${value['prix']}</strong></div>
                                    <div class="small-12 medium-12 large-2 tar"><strong>${value['total']} <i>F cfa</i></strong></div>
                                  </div>`)
                    });

                    $('.total').append(`<div class="row">
                                            <div class="small-12 medium-12 large-12 tar">
                                            <em>Total à payer</em>: <strong>${data['total']}<i>F cfa</i></strong>
                                            </div>
                                          </div>`);

//                        $('#show-item-commande').on('hidden.bs.modal', function(e)
//                        {
//                            $(this).removeData();
//                        }) ;

                },
            });
        });

        $('#show-commande').submit(function (e) {
            e.preventDefault();
            $('#show-item-commande').on('hidden.bs.modal', function(e)
            {
                alert('hello');
                $(this).removeData();
            }) ;

            $('#show-item-commande').modal('close');
        });


    }
</script>
@endsection

