@extends('layouts.admin.master-admin')
@section('title')
    catégories
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')
    <main class="main">
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Label</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->libelle}}</td>
                            <td> <a href="/admin/categorie/{{$category->id}}/show" class="modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                                <a href="/admin/categorie/{{$category->id}}/edit" class="modal-trigger"><i class="material-icons">edit</i></a>
                                <a href="/admin/categorie/{{$category->id}}/delete" class="modal-trigger" onclick="return confirm('Etes vous sur de voulloir supprimer cette categorie?');"><i class="material-icons">delete</i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $categories->appends(request()->query())->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection
