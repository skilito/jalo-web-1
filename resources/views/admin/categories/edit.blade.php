@extends('layouts.admin.master-admin')
@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="/../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Edit Catégorie</h4>
                </header>
                <p>&nbsp;</p>
                <form action="/admin/categorie" id="saveEditCategorie" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="{{$categorie->id}}" name="categorie_id">
                    <div class="row">
                        <div class="small-12 medium-12 large-12">
                            <label for="editNom" data-error="...">Nom Catégorie *</label>
                            <div class="input-field">
                                <input id="nom_categorie" type="text" class="validate" name="nom_categorie"
                                       value="{{$categorie->libelle}}">
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-12">
                            <label for="editFournisseur" data-error="...">Visibilité *</label>
                            <div class="input-field-custom">
                                <select id="visible" name="visible">
                                    @if($categorie->visible == true)
                                        <option value="0">Visible</option>
                                    @else
                                        <option value="0">Non Visible</option>
                                    @endif
                                    @foreach($visibles as $visible)
                                        <option value="{{ $visible['index'] }}">{{ucfirst($visible['nom'])}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        {{--<p>&nbsp;</p>--}}
                        <div class="small-12 medium-12 large-12">
                            <div class="file-field input-field">
                                <span>Changer icone</span>
                                <input type="file" name="new_icone">
                            </div>
                        </div>

                    </div>
                </form>
                <div class="form-footer">
                    <a href="/admin/categories" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                    <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button"
                            form="saveEditCategorie" type="submit" data-disable-with="<i class='fa fa-spinner fa-spin'></i> loading.....">Enregistrer
                    </button>
                </div>
            </div>
        </div>

    </main>

@section('script')
    <script src="/../js/vendors/jquery.min.js"></script>
    <script src="/../js/vendors/foundation.min.js"></script>
    <script src="/../js/vendors/materialize.min.js"></script>
    <script src="/../js/app.js"></script>
    <script src="/../js/vendors/flickity.pkgd.min.js"></script>
@endsection

@endsection



