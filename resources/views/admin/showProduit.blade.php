
@extends('layouts.admin.master-admin')

@section('title')
<<<<<<< HEAD
    JALÔ | Detail  produit
=======
    JALÔ - Tableau de bord
>>>>>>> dev
@endsection
@section('app-css')
    <link rel="stylesheet" href="../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>
        <!-- Détails des produits -->

        <div class="row">
            <div class="small-12 medium-12 large-8 large-offset-2">
                <div class="card card-single-product">
                    <div class="about-the-author">
                        {{--<h3 class="author-title">Produit</h3>--}}
                        <div class="row">
                            <div class="small-12 medium-6 columns">

                                <div class="main-carousel-product">
                                    <div class="carousel-cell">
                                        <img src="{{url('/')}}/{{$catalogue->photo}}" alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="small-12 medium-6 columns">
                                <h4 class="separator-left">{{ucfirst($catalogue->nomProduit)}}</h4>
                                @if($catalogue->description != null)
                                    <p>{{ucfirst($catalogue->description)}}</p>
                                @else
                                    <p>Pas de description pour ce produit</p>
                                @endif
                                {{--<a href="#" class="add-bucket-btn waves-effect waves-black"> Acheter </a>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!-- Flickity Slider -->
    @section('script')
        <script src="../js/vendors/jquery.min.js"></script>
        <script src="../js/vendors/foundation.min.js"></script>
        <script src="../js/vendors/materialize.min.js"></script>
        <script src="../js/app.js"></script>
        <script src="../js/vendors/flickity.pkgd.min.js"></script>
    @endsection
@endsection


