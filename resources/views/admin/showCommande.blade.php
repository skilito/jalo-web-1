
@extends('layouts.admin.master-admin')
@section('title')
<<<<<<< HEAD
    JALÔ | Detail Commande
=======
    JALÔ - Tableau de bord
>>>>>>> dev
@endsection
@section('app-css')
    <link rel="stylesheet" href="../../css/app.css">
@endsection
@section('content')
    <p>&nbsp;</p>
    <div class="row">
        @if(count($produits) == 0)
            <h4>Pas de produit pour cette commande</h4>
        @else
        <div class="small-12 medium-12 large-6 large-offset-3">
            <div class="row p-10">
                <div class="small-12 medium-12 large-12">
                    <h4>Confirmation</h4>
                </div>

                <div class="small-12 medium-12 large-12">
                    <p>&nbsp;</p>
                    <div class="row" style="margin-left: -0.10035rem;">
                        <div class="small-12 medium-6 large-16">
                            <h4>{{ utf8_decode($commande->prenom) }} {{ utf8_decode($commande->nom) }}</h4>
                        </div>
                        <div class="small-12 medium-6 large-16">
                            <date>{{ $commande->date_commande }}</date>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>

            <table>
                <thead>
                    <tr>
                        <th>Produits</th>
                        <th>Quantite</th>
                        <th>Prix</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($produits as $produit)
                            <tr>
                                <td>{{ucfirst($produit->libelle)}}</td>
                                <td>{{$produit->quantite}}</td>
                                <td>{{ $produit->prix }}</td>
                                <td>{{$produit->total}}</td>
                            </tr>
                        @endforeach
                </tbody>
                <tfooter>
                    <tr>
                        <td colspan="4"><em>Total à payer</em>: <strong>{{$total_commande}} <i>F cfa</i></strong></td>
                    </tr>
                </tfooter>
            </table>

            <div class="">
                <a href="/admin/commandes" class="modal-action modal-close waves-effect waves-black btn-flat grey button">FERMER</a>
                @if ($commande->etat == "pay_boutiquier")
                    <a href="/commande?commande_id={{$commande->id}}&status=pay_jalo" class="modal-action modal-close waves-effect waves-black btn-flat success button">Confirmer paiement</a>
                @elseif ($commande->etat == "pay_jalo")
                    <a href="/commande?commande_id={{$commande->id}}&status=livre" class="modal-action modal-close waves-effect waves-black btn-flat success button">Livrer</a>
                @endif
            </div>
        </div>
        @endif
    </div>

@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection