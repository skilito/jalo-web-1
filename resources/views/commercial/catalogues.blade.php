@extends('layouts.commercial.master-commercial')
@section('content')
    <main class="main">
        <p>&nbsp;</p>
        <!-- Liste des produits -->
        <div class="row">
            @foreach($catalogues as $catalogue)
                <div class="small-12 medium-6 large-3 p-10">
                    <div class="card card-center">
                        <img src="{{url('/')}}/{{$catalogue->photo}}" alt="">
                        <div class="card-section">
                            <h4 class="card-title">{{ ucfirst($catalogue->produit) }}</h4>
                            <small class="card-sub-title">{{ ucfirst($catalogue->prenom) }} {{$catalogue->nom}}</small>
                            <strong class="card-price">{{$catalogue->prix}}  <span>F cfa</span></strong>
                            <a href="#edit-product" class="btn-floating btn-tiny waves-effect waves-yellow yellow btn-edit modal-trigger" onclick="showModalEditProduct({{$catalogue->id}})">
                                <i class="material-icons yellow">edit</i>
                            </a>
                            <form class="delete-product">
                                <input type="hidden" name="catalogue_id" value="{{$catalogue->id}}" class="catalogue_id">
                                {{ csrf_field() }}
                                <button class="btn-floating btn-tiny waves-effect waves-yellow yellow btn-delete"
                                        onclick="return confirm('Etes vous sur de voulloir supprimer ce produit?');" type="submit">
                                    <i class="material-icons red">delete</i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                    {{$catalogues->links()}}
                </ul>
            </div>
        </div>
        {{--edit product--}}
        <div id="edit-product" class="modal modal-fixed-footer">
            <div class="modal-content">
                <header class="header-modal">
                    <h4>Edit Produit</h4>
                </header>
                <p>&nbsp;</p>
                <form action="" id="saveEditProduct">
                    <div class="row">
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editNom"  data-error="..." >Nom Produit *</label>
                            <div class="input-field">
                                <input id="editNom" type="text" class="validate"  name="editNom">
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editPrice"  data-error="..." >Prix *</label>
                            <div class="input-field">
                                <input id="editPrice" type="number" class="validate"  name="editPrice">
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <label for="editFournisseur"  data-error="..." >Fournisseur *</label>
                            <div class="input-field-custom">
                                <select id="fournisseur-edit" name="editFournisseur">
                                    <option value="0"></option>
                                    @foreach($fournisseurs as $fournisseur)
                                        <option value="{{$fournisseur->id}}">{{ucfirst($fournisseur->prenom)}} {{ucfirst($fournisseur->nom)}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="small-12 medium-12 large-8 large-offset-2">
                            <div class="row no-margin">
                                <div class="small-12 medium-12 large-5">
                                    <label for="EditPourcentageJalo"  data-error="..." >Pourcentage JALO</label>
                                    <div class="input-field">
                                        <input id="EditPourcentageJalo" type="number" class="validate"  name="EditPourcentageJalo">
                                    </div>
                                </div>
                                <div class="small-12 medium-12 large-5 large-offset-2">
                                    <label for="EditPourcentageBoutiquier"  data-error="..." >Pourcentage Boutiquier</label>
                                    <div class="input-field">
                                        <input id="EditPourcentageBoutiquier" type="number" class="validate"  name="EditPourcentageBoutiquier">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-6 large-offset-2">
                            <div class="file-field input-field">
                                <div class="btn lh13 bgc-yellow">
                                    <span>Photo Produit</span>
                                    <input type="file" name="new_photo">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ANNULER</a>
                <button href="#!" class="modal-action waves-effect waves-black btn-flat grey button" form="saveEditProduct" type="submit">MODIFIER</button>
            </div>
        </div>
    </main>
@endsection
<script src="../js/vendors/jquery.min.js"></script>
<script>

    $('.delete-product').on('submit', function(event)
    {
        alert('ok');
        event.preventDefault();

        var formData = new FormData($(this)[0]);

        $(document).ready(function()
        {
            $.ajax({
                url: '/admin/product/delete',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });

        });
    })

    $(document).ready(function(){

        $('#saveProduit').on('submit', function(event)
        {
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: 'admin/produit',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {

                    $('#add-product').modal('close')
                    location.reload();
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })


    function showModalEditProduct(catalogue_id)
    {
        $(document).ready(function()
        {
            $.post('/admin/produit/edit',
                {
                    catalogue_id: catalogue_id,
                    "_token": $("input[name=_token]").val()
                }, function(data)
                {
                    $('#saveEditProduct .row  .input-field #editNom').val(data[0].nomProduit);
                    $('#saveEditProduct .row  .input-field #editPrice').val(data[0].prix);
                    $('#saveEditProduct .row  .input-field #EditPourcentageJalo').val(data[0].pourcentage_jalo);
                    $('#saveEditProduct .row  .input-field #EditPourcentageBoutiquier').val(data[0].pourcentage_boutiquier)
                    $("#fournisseur-edit option[value= \"0\"]").text(data[0].fournisseur_prenom.replace(/\b\w/g, function(l){
                        return l.toUpperCase() }) + ' '+ data[0].fournisseur_nom.replace(/\b\w/g, function(l){ return l.toUpperCase()
                    }));
                });


            $('#saveEditProduct').on('submit', function(event){
                event.preventDefault();

                var formData = new FormData($(this)[0]);

                console.log(formData);

                formData.append('catalogue_id', catalogue_id);

                $.ajax({
                    url: '/admin/product/update',
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#edit-product').modal('close')
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            })

        });
    }

    $('.delete-product').on('submit', function(event){
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        alert('merci')
//        $(document).ready(function()
//        {
//            $.ajax({
//                url: '/admin/product/delete',
//                type: 'POST',
//                data: formData,
//                async: false,
//                success: function (data) {
//                    location.reload();
//                },
//                cache: false,
//                contentType: false,
//                processData: false
//            });
//
//        });
    })

</script>