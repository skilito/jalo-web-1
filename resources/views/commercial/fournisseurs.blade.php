@extends('layouts.commercial.master-commercial')
@section('content')
    <main class="main">

        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-12 large-12">
                <table class="unstriped">
                    <thead>
                    <tr>
                        <th>Prénom & Nom</th>
                        <th>Quartier</th>
                        <th>Adresse</th>
                        <th>Téléphone</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($fournisseurs as $fournisseur)
                        <tr>
                            <td>{{ ucfirst($fournisseur->prenom) }} {{ ucfirst($fournisseur->nom) }}</td>

                            <td>{{ ucfirst($fournisseur->quartier) }}</td>
                            <td> {{ ucfirst($fournisseur->adresse) }}</td>
                            <td> {{ $fournisseur->phone1 }}</td>
                            {{--<td><a href="#edit-fournisseur" class="modal-trigger" onclick=""><i class="material-icons">remove_red_eye</i></a></td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="small-12 medium-12 large-12">
                        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                            {{ $fournisseurs->links() }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@section('script')
    <script src="../../js/vendors/jquery.min.js"></script>
    <script src="../../js/vendors/foundation.min.js"></script>
    <script src="../../js/vendors/materialize.min.js"></script>
    <script src="../../js/app.js"></script>
@endsection
@endsection