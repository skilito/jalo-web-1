@extends('layouts.app')

@section('content')

    <div class="sign-page">
        <div class="sign-page-left" style="overflow: hidden;">
            <div class="main-carousel">
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 1</h2>
                    </div>
                </div>
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 2</h2>
                    </div>
                </div>
                <div class="carousel-cell">
                    <img src="images/background-slider.png" alt="">
                    <div class="text-filter">
                        <h2 class="text-filter-title">Tous près de chez vous Slider - 3</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="sign-page-right">
            <img src="images/logo-black.jpeg" alt="" class="logo">
            <header class="header-sign">
                <a href="/register" class="hollow button secondary">Ouvrir un compte</a>
            </header>
            <div class="form-sign-wrapper">
                <div class="form-sign">
                    <h4>Connexion</h4>
                    <form action="" id="login">
                        {{ csrf_field() }}
                        <div class="input-field">
                            <label for="phone1"  data-error="votre adresse téléphone est invalide" >Votre numéro de téléphone</label>
                            <input id="phone1" type="tel" class="validate" name="phone1" required >
                        </div>
                        <div class="input-field">
                            <label for="password"  data-error="Votre mot de passe est incorect">Mot de passe *</label>
                            <input id="password" type="password" class="validate" name='password'>
                        </div>
                        <div class="error">
                        </div>
                        <p>&nbsp;</p>
                        <div class="footer-form-button">
                            <button class="button secondary" type="submit" value="Submit" form="login" >Connexion</button>
                            <a href="/register" class="link-to-sign">Je n'ai pas encore de compte</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="js/vendors/jquery.min.js"></script>
{{--<script src="js/vendors/foundation.min.js"></script>--}}
{{--<script src="js/vendors/materialize.min.js"></script>--}}
{{--<script src="js/app.js"></script>--}}

<script>

    $(document).ready(function(){

        $('#login').on('submit', function(event){
            event.preventDefault();

           var formData = new FormData($(this)[0]);

            $.ajax({
                url: '/login',
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {

                    if(data=='error')
                    {
                        if($(".error").children().length < 1) {
                            $('.error').append("<label style='color: red'>Compte utilisateur ou mot de passe incorrect. Réessayez !</label>")
                        }
                    }
                    else if(data == 'admin')
                    {
                        window.location.href = "admin/dashboard";
                    }
                    else if(data == 'commercial')
                    {
                        window.location.href = "commercial/catalogues?category=all";
                    }
                    else
                    {
                        window.location.href = "client/catalogues?category=all";
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        })
    })
</script>
