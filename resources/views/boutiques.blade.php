@extends('layouts.app')
@section('content')
    <div class="preview">

        <div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
            <button class="menu-icon" type="button" data-toggle="example-menu"></button>
            <div class="title-bar-title">Menu</div>
        </div>
        <header class="header-home">
            <div class="row">
                <div class="top-bar" id="example-menu">
                    <a href="/"><img src="./images/logo-yellow.jpeg" alt="Logo JALO" class="logo-home"></a>
                    <div class="top-bar-right">
                        <ul class="menu">
                            {{--<li><a href="/login" class="underline-hover" >Se connecter</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <p>&nbsp;</p>
        <div class="row">
            @foreach($boutiques as $boutique)
            <div class="small-12 medium-6 large-4 p-10">
                <div class="card card-profil">
                    <div class="card-section">
                        <div class="seller">
                            <div class="seller-details">
                                <h4>{{ucfirst($boutique->prenom)}} {{ucfirst($boutique->nom)}}</h4>
                                <ul>
                                    <li><p><i class="material-icons">Adresse</i><span>{{ str_limit(ucfirst($boutique->adresse), 40)}}</span></p></li>
                                    <li><p><i class="material-icons">Téléphone</i><span>{{$boutique->phone1}}</span></p></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>

            @endforeach
        </div>
        <p>&nbsp;</p>

        <div class="row">
            <div class="small-12 medium-12 large-12">
                <ul class="pagination text-center" role="navigation" aria-label="Pagination">
                    {{ $boutiques->appends(request()->query())->links() }}
                </ul>
            </div>
        </div>

        <footer class="footer">
            <div class="row">

                <div class="small-12 medium-6 large-4">
                    <div class="footer-brand">
                        <img src="images/logo-yellow.jpeg" alt="">
                        <p>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines.</p>
                        {{--<a href="https://play.google.com/store?hl=en" target="_blank" class="mobile"><img src="./images/android-download.svg" alt=""></a>--}}
                    </div>
                </div>

                <div class="small-12 medium-6 large-3 large-offset-2">
                </div>
                <div class="small-12 medium-6 large-3">
                    <h4 class="section-title">Nous contacter</h4>
                    <address>
                        <strong>85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal</strong>
                        <br>
                        <span>+221 33 824 81 95  <i>&nbsp;</i> | <i>&nbsp;</i> 77 225 94 32 <i>&nbsp;</i> | <i>&nbsp;</i> 78 384 01 04 </span>
                        <br>
                        <span>info@jaloshops.com</span>
                    </address>
                    <ul class="menu horizontal">
                        <li><a href="https://www.facebook.com/jalo.jalo.1297943" target="_blank"><img src="images/social-network/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer>

    </div>

    {{--<script src="js/vendors/jquery.min.js"></script>--}}
    {{--<script src="js/vendors/foundation.min.js"></script>--}}
    {{--<script src="../js/vendors/materialize.min.js"></script>--}}
    {{--<script src="js/app.js"></script>--}}
@endsection

