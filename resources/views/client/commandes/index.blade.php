@extends('layouts.master_client')
@section('content')
<main class="main">
      <div class="row">
        <header class="header-main-content">
          <div class="top-bar">
            <div class="top-bar-left">
              <ul class="menu">
                <li> <a href="" class="button">Toutes les catégories</a> </li>
                <li> <a href="" class="hollow button secondary">Matériels électorinique</a> </li>
                <li> <a href="" class="hollow button secondary">Vétements</a> </li>
                <li> <a href="" class="hollow button secondary">Consomation courante</a> </li>
              </ul>
            </div>
            <div class="top-bar-right pos-rltv">
              <div class="fixed-action-btn horizontal">
                <a class="waves-effect waves-yellow btn-floating btn-large black">
                  <i class="material-icons">add_shopping_cart</i>
                </a>
                <!-- Modal Trigger -->
                <ul>
                  <li>
                    <a class="waves-effect waves-black btn-floating green darken-1 modal-trigger" href="#create-shop" data-position="bottom" data-delay="50"  data-tooltip="Faire une commande">
                      <i class="material-icons">add_shopping_cart</i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </header>
      </div>
      
      <div class="row">
        <table class="unstriped">
          <thead>
            <tr>
                <th>Nom produit</th>
                <th>Total</th>
                <th>Boutiquier</th>
                <th>Téléphone</th>
                <th>Action</th>
            </tr>
          </thead>

          <tbody>

            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>
            <tr>
              <td>Nom d'un produit</td>
              <td><strong>26 000 F cfa</strong></td>
              <td>Nom & Prénom du boutiquier</td>
              <td>7* - 123 - 45 - 67</td>
              <td> 
                <a href="#show-commande" class="action-in-table modal-trigger"><i class="material-icons">remove_red_eye</i></a>
                <a class="action-in-table" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')"><i class="material-icons">close</i></a>
              </td>
            </tr>

          </tbody>
        </table>
              
      </div>

      <div class="row">
        <div class="small-12 medium-12 large-12">
          <ul class="pagination text-center" role="navigation" aria-label="Pagination">
            <li class="pagination-previous disabled">Précédent</li>
            <li class="current"><span class="show-for-sr">Page courante</span> 1</li>
            <li><a href="#" aria-label="Page 2">2</a></li>
            <li><a href="#" aria-label="Page 3">3</a></li>
            <li><a href="#" aria-label="Page 4">4</a></li>
            <li class="ellipsis"></li>
            <li><a href="#" aria-label="Page 12">12</a></li>
            <li><a href="#" aria-label="Page 13">13</a></li>
            <li class="pagination-next"><a href="#" aria-label="Next page">Suivant</a></li>
          </ul>
        </div>
      </div>


      <!-- Modal Structure -->
      <div id="show-commande" class="modal modal-fixed-footer">
        <div class="modal-content">
          <header class="header-modal">
            <h4>Commande 89SCS5CQ</h4>
          </header>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-4"><p>Produits</p></div>
            <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
            <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
            <div class="small-12 medium-12 large-2 tar"><p>Tatal</p></div>
            <div class="small-12 medium-12 large-2 tar"><p>Annuler</p></div>
          </div>
          <div class="bucket-items">
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate" disabled>
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate" disabled>
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate" disabled>
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate" disabled>
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate" disabled>
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
          </div>
          <div class="total-paid">
            <div class="row">
              <div class="small-12 medium-12 large-6 large-offset-4 tar">
                <em>Total à payer</em>: <strong>1 600 <i>F cfa</i></strong>
              </div>
            </div>
          </div>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-6 large-offset-6">
              <ul class="shop-info">
                <li><p>Nom boutiquier</p><strong>Nom du boutiquier</strong></li>
                <li><p>Quartier</p><strong>Nom du quartier</strong></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button" onclick="Materialize.toast('La commande 37DHS7 a été annuler', 4000, 'rounded')">ANNULER LA COMMANDE</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">FERMER</a>
        </div>
      </div>
      <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h4>Nouvelle vente</h4>
          <div class="row">
            <div class="small-12 medium-12 large-8 p-r-30">
              <div class="input-field">
                <input id="last_name" type="text" class="validate">
                <label for="last_name"  data-error="ce client n'existe pas" >Nom du client</label>
              </div>
            </div>
            <div class="small-12 medium-12 large-4">
              <button class="add-bucket-btn waves-effect waves-black w-100 modal-trigger" href="#create-user"">
                Créer client <span class="btn-floating btn-tiny waves-effect waves-yellow white"><i class="material-icons">person_add</i></span>
              </button>
            </div>
          </div>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-6"><p>Produits</p></div>
            <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
            <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
            <div class="small-12 medium-12 large-2"></div>
          </div>

          <div id="product-list">
            <div class="row product-list-item">
              <div class="small-12 medium-12 large-6 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="text" class="validate">
                  <label for="last_name"  data-error="ce produit n'existe pas" >Nom produit</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate">
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate">
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Prix unitailre</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10 tar">
                <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-12 large-12 p-r-10 tar">
              <a class="waves-effect waves-black hollow button secondary" id="add-more">Ajouter un produit</a>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
          <a class="modal-action modal-close waves-effect waves-black btn-flat success button modal-trigger" href="#facture-vente">VALIDER</a>
        </div>
      </div>

      <div id="bucket-container" class="modal modal-fixed-footer">
        <div class="modal-content">
          <header class="header-modal">
            <h4>Mon panier</h4>
          </header>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-4"><p>Produits</p></div>
            <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
            <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
            <div class="small-12 medium-12 large-2 tar"><p>Tatal</p></div>
            <div class="small-12 medium-12 large-2 tar"><p>Annuler</p></div>
          </div>
          <div class="bucket-items">
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate">
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate">
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate">
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate">
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
            <div class="row">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                  <img src="https://source.unsplash.com/collection/190727" alt="">
                  <strong>Nom produit</strong>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                    <input id="last_name" type="number" value="2" class="validate">
                  </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10"><strong>800</strong></div>
              <div class="small-12 medium-12 large-2 tar"><strong>1 600</strong></div>
                <div class="small-12 medium-12 large-2 p-r-10 tar">
                  <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
                </div>
            </div>
          </div>
          <div class="total-paid">
            <div class="row">
              <div class="small-12 medium-12 large-6 large-offset-4 tar">
                <em>Total à payer</em>: <strong>1 600 <i>F cfa</i></strong>
              </div>
            </div>
          </div>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-6 large-offset-6">
              <ul class="shop-info">
                <li><p>Nom boutiquier</p><strong>Nom du boutiquier</strong></li>
                <li><p>Quartier</p><strong>Nom du quartier</strong></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER VENTE</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">Valider</a>
        </div>
      </div>

      <div id="create-user" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h4>Nouveau client</h4>
          <div class="row">
            <div class="small-12 medium-12 large-8 large-offset-2">
              <div class="input-field">
                <input id="last_name" type="text" class="validate">
                <label for="last_name"  data-error="ce produit n'existe pas" >Prénom *</label>
              </div>
            </div>
            <div class="small-12 medium-12 large-8 large-offset-2">
              <div class="input-field">
                <input id="last_name" type="text" class="validate">
                <label for="last_name"  data-error="ce produit n'existe pas" >Nom *</label>
              </div>
            </div>
            <div class="small-12 medium-12 large-8 large-offset-2">
              <div class="input-field">
                <input id="last_name" type="tel" class="validate">
                <label for="last_name"  data-error="numéro email invalide" >Téléphone *</label>
              </div>
            </div>
            <div class="small-12 medium-12 large-8 large-offset-2">
              <div class="input-field">
                <input id="last_name" type="email" class="validate">
                <label for="last_name"  data-error="adresse email invalide" >email *</label>
              </div>
            </div>
            <div class="small-12 medium-12 large-8 large-offset-2">
              <div class="input-field">
                <select>
                  <option value="" disabled selected>Sélectioner son quartier</option>
                  <option value="1">Maristes</option>
                  <option value="2">Almadies</option>
                  <option value="3">Pikine</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">CRÉER CLIENT</a>
        </div>
      </div>

      <div id="create-shop" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h4>Nouvelle commande</h4>
          <h5>Sélectioner les produits</h5>
          <p>&nbsp;</p>
          <div class="row">
            <div class="small-12 medium-12 large-4"><p>Produits</p></div>
            <div class="small-12 medium-12 large-2"><p>Quantité</p></div>
            <div class="small-12 medium-12 large-2"><p>Prix unitaire</p></div>
            <div class="small-12 medium-12 large-2"><p>Total</p></div>
            <div class="small-12 medium-12 large-2"></div>
          </div>

          <div id="commande-list">
            <div class="row commande-list-item">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="text" class="validate" value="Nom produit">
                  <label for="last_name"  data-error="ce produit n'existe pas" >Nom produit</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate" value="1">
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate" value="390" disabled>
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Prix unitailre</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate" value="390" disabled>
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Total</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10 tar">
                <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
              </div>
            </div>
            <div class="row commande-list-item">
              <div class="small-12 medium-12 large-4 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="text" class="validate">
                  <label for="last_name"  data-error="ce produit n'existe pas" >Nom produit</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate">
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Quantité</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate" value="0" disabled>
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Prix unitailre</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10">
                <div class="input-field">
                  <input id="last_name" type="number" class="validate" value="0" disabled>
                  <label for="last_name"  data-error="ce champ ne prend que des chiffres" >Total</label>
                </div>
              </div>
              <div class="small-12 medium-12 large-2 p-r-10 tar">
                <button class="btn-floating btn-tiny waves-effect waves-black red" id="remove-this-item"><i class="material-icons">close</i></button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-12 large-12 p-r-10 tar">
              <a class="waves-effect waves-black hollow button secondary" id="add-more-commande-client">Ajouter un produit</a>
            </div>
          </div>

          <div class="total-paid">
            <div class="row">
              <div class="small-12 medium-12 large-6 large-offset-4 tar">
                <em>Total à payer</em>: <strong>390 <i>F cfa</i></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER COMMANDE</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-black btn-flat success button">ENVOYER LA COMMANDE</a>
        </div>
      </div>
    </main>
@endsection
