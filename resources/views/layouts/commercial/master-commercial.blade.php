<!doctype html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jalo</title>
    <link rel="stylesheet" href="../css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

@include('layouts.commercial.header-commercial')

@yield('content')

<script src="../js/vendors/jquery.min.js"></script>
<script src="../js/vendors/awesomplete.min.js"></script>
<script src="../js/vendors/foundation.min.js"></script>
<script src="../js/vendors/materialize.min.js"></script>
<script src="../js/app.js"></script>

</body>
</html>

