<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = ['nom','prenom','adresse','phone1','phone2',
        'date_creation','quartier_id', 'boutiquier_id'];


    public $timestamps = false;

    public static function getClientsByBoutiquier($boutiquier_id)
    {
       return DB::table('clients as c')->select('c.nom', 'c.prenom', 'c.adresse', 'c.phone1',
                    'c.phone2', 'c.date_creation', 'q.nom as quartier', 'c.id')
              ->join('quartiers as q', 'q.id', '=','c.quartier_id')
              ->where('c.boutiquier_id', $boutiquier_id)->get();
    }

    public static  function  getClientsByCommercial($commercial)
    {

    }
}
