<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Vente extends Model
{
    use Sluggable;

    protected $table = 'ventes';
    protected $fillable = ['client_id', 'reference', 'date_vente','type_id', 'slug', 'etat_id'];
    public $timestamps = false;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'reference'
            ]
        ];
    }

}
