<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    protected $table = 'etats';
    protected $fillable = ['id','libelle'];
    public $timestamps = false;
}
