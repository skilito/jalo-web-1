<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Marchandise extends Model
{
    use Sluggable;

    protected $table = 'marchandises';
    protected $fillable = ['id', 'vendeur_id','prix','date_ajout','quantite_dispo', 'produit_id'];
    public $timestamps = false;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'vendeur_id'
            ]
        ];
    }
}
