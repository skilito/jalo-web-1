<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['id','role'];
    public $timestamps = false;

    public static function getRoles()
    {
        return self::all();
    }

    public static function getRoleId($role)
    {
        if($role == 'admin'){
            $role_id = self::where('role', $role)->first()->id;
        }
        elseif($role == 'boutiquier')
        {

            $role_id = self::where('role', ''.$role)->first()->id;
        }

        elseif($role == 'commercial')
        {

            $role_id = self::where('role', ''.$role)->first()->id;
        }
        elseif($role == 'fournisseur')
        {

            $role_id = self::where('role', ''.$role)->first()->id;
        }
        else
        {
            $role_id = self::where('role', 'client')->first()->id;
        }
        return $role_id;
    }

    public static function getRoleById($role_id)
    {
        return self::find($role_id)->role;
    }
}
