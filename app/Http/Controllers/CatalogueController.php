<?php

namespace App\Http\Controllers;
use App\Catalogue;

use Illuminate\Http\Request;
use App\Categorie;
use App\Panier;
use \Auth;
use Illuminate\Support\Facades\DB;
use App\Boutiquier;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Produit;

class CatalogueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $categories = Categorie::getAllCategories();

        $catalogues = Catalogue::getAllCatalogue($request->get('category'));

        $nbr_panier = Panier::where('user_id', Auth::user()->id)->count();

        $produitsList = DB::table('produits as p')->select('p.libelle', 'c.prix', 'panier.id as panier_id')
                    ->join('catalogues as c','c.produit_id', '=', 'p.id')
                    ->join('panier', 'panier.catalogue_id', '=', 'c.id')
                    ->where('panier.user_id', '=', Auth::user()->id)->get();

        $boutiquier = DB::table('users')->select('users.nom', 'users.prenom', 'quartiers.nom as quartier')
                        ->join('boutiquiers', 'boutiquiers.client_id', '=', 'users.id')
                        ->join('quartiers', 'quartiers.id', '=', 'users.quartier_id')->
                        where('client_id', Auth::user()->id)->get()->first();

        $products = DB::table('catalogues')->select('catalogues.id as catalogue_id', 'produits.libelle','produits.description')
                    ->join('produits', 'produits.id', '=', 'catalogues.produit_id')->get();


        //return response()->json($products);
        return view('client.catalogues')->with(['catalogues' => $catalogues])
            ->with(['categories' => $categories])->with(['nbr_panier' => $nbr_panier])
            ->with(['produitsList' => $produitsList])->with(['boutiquier' => $boutiquier])
            ->with(['products' => $products]);
    }

    public function getPrice(Request $request)
    {
        $catalogue_id = $request->get('catalogue_id');
        $price = Catalogue::where('id', (int)$catalogue_id)->first()->prix;
        return response()->json($price);
    }
   
}
