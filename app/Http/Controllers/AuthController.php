<?php

namespace App\Http\Controllers;
use App\Utilities\Utility;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use App\User;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    } */

    public function login(Request $request){

        $rules = array(
            'password'   => 'required',
            'phone1'   => 'required'
        );

        $credentials['phone1'] = $request->get('phone1');
        $credentials['password'] = $request->get('password');

        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return response()->json(['error' =>$validator->errors()], 404);
        }

        try {

            if (!$token = JWTAuth::attempt($credentials)) {

                return response()->json(['message' =>'invalid_email_or_password'], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['message' => 'failed_to_create_token'], 500);
        }
        $userAuth = JWTAuth::toUser($token);

        $user = ['nom' => $userAuth->nom, 'prenom' => $userAuth->prenom, 'email' => $userAuth->email,
            'sexe' => $userAuth->sexe, 'adresse' => $userAuth->adresse, 'role_id' =>$userAuth->role_id,
            'info_id' => $userAuth->info_id, 'quartier_id' => $userAuth->quartier_id,
            'date_inscription' => $userAuth->date_inscription, 'date_naissance' => $userAuth->date_naissance,
            'phone1' => $userAuth->phone1, 'phone2' =>$userAuth->phone2, 'avatar' => $userAuth->avatar];

        return response()->json(['token' => $token, 'user' => $user]);
    }

    public function register(Request $request){

        $user = $request->all();

        $rules = array(
            'nom'       => 'required|min:2',
            'prenom'     => 'required|min:2',
            'quartier_id'   => 'required',
            'password'   => 'required|min:5',
            'role_id'   => 'required',
            'info_id'   => 'required',
            'phone1'   => 'required|unique:users|min:9'
        );
        $validator = Validator::make($user, $rules);

        if ($validator->fails()) {
            return response()->json(['error' =>$validator->errors()], 404);
        }
        else {

            $user['password'] = bcrypt($request->get('password'));
            $user['date_inscription'] = Carbon::now();

             if(User::create($user))
             {
                 $userQuery = DB::table('users')->select('nom', 'prenom', 'sexe', 'phone1', 'avatar', 
                     'date_naissance','phone2','role_id','info_id','date_inscription','email', 'quartier_id')
                     ->where('phone1',$user['phone1'])
                     ->first();

                return response()->json(['message' => 'User created successfully', 'data' => $userQuery ]);
            }
            else
            {
                return response()->json(['status' => false, 'message' => 'User created successfully', 'data' => $user]);
            }


        }
    }

    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));
        return response()->json(['result' => $user]);
    }


}


