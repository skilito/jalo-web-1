<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;
use App\Produit;
use JWTAuth;

class ProduitController extends Controller
{
    public function getListProductToCategory(Request $request)
    {
        $category =$request->get('category');

        if($category)
        {
            $produits =  Categorie::where('id', $category)->firstOrFail()->produits()->get();

            return response()->json(['produits' => $produits, 'status' => 200]);
        }
        else
            {
            return response()->json(['message' => 'parameter category not present', 'status' => 404]);
        }
    }

    public function show(Request $request){

        $product = $request->get('product');

        if($product)
        {
            $products = Produit::getProduit($product);

            return response()->json(['product' => $products ,'status' =>  200]);
        }
        else
        {
            return response()->json(['message' => 'the product does not exist', 'status' => 404]);
        }

    }

    public function getListProductsUser(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));
        $produits = Produit::getListProduitToUser($user->id);
        return response()->json(['produits' => $produits, 'status' => 200]);
    }

    public function autoComplete(Request $request)
     {
        //dd("autoComplete ",$request->get('term',''));
        $query = $request->get('term','');
        
        $products=Product::where('libelle','LIKE','%'.$query.'%')->get();

        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->libelle,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

}
