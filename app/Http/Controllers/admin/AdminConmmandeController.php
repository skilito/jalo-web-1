<?php

namespace App\Http\Controllers\Admin;
use App\CatalogueCommande;
use App\Commande;
use App\Etat;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Categorie;
use App\User;
use App\Role;

class AdminConmmandeController extends Controller
{
    public function index(Request $request)
    {
        $categoties = Categorie::all();

        $commerciaux = User::where('role_id', Role::getRoleId('commercial'))->get();

        $fournisseurs = User::where('role_id', Role::getRoleId('fournisseur'))->get();

        $search = $request->get('search');

        if($search != null)
        {
            $commandes = DB::table('commandes as c')->select('c.id', 'c.date_commande', 'c.reference', 'client.nom as nomClient',
                'client.prenom as prenomClient', 'client.phone1 as phoneClient', 'bou.nom as nomBoutiquier',
                'bou.prenom as prenomBou', 'bou.phone1 as telBou', 'etats.description as etat', 'etats.libelle as libelle_etat',
                'commercial.prenom as prenomCom', 'commercial.nom as nomCom', 'commercial.phone1 as telCom', 'c.call', 'c.date_livraison_boutique', 'c.date_livraison_client')->
            leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')->
            join('clients as client', 'client.id', '=', 'c.client_id')->
            leftjoin('users as commercial', 'commercial.id', '=', 'b.commercial_id')->
            join('users as bou', 'bou.id', '=', 'c.boutiquier_id')->
            join('etats', 'etats.id', '=', 'c.etat_id')
                ->where('c.reference', $search)
                ->orWhere('client.prenom', 'like', '%' . $search . '%')
                ->orWhere('client.nom', 'like', '%' . $search . '%')
                ->orWhere('bou.prenom', 'like', '%' . $search . '%')
                ->orWhere('bou.nom', 'like', '%' . $search . '%')
                ->orderBy('date_commande', 'DESC')->paginate(16);
        }

        else {

            $commandes = DB::table('commandes as c')->select('c.id', 'c.date_commande', 'c.reference', 'client.nom as nomClient',
                'client.prenom as prenomClient', 'client.phone1 as phoneClient', 'bou.nom as nomBoutiquier',
                'bou.prenom as prenomBou', 'bou.phone1 as telBou', 'etats.description as etat','etats.libelle as libelle_etat',
                'commercial.prenom as prenomCom', 'commercial.nom as nomCom', 'commercial.phone1 as telCom', 'c.call', 'c.date_livraison_boutique', 'c.date_livraison_client')->
            leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')->
            join('clients as client', 'client.id', '=', 'c.client_id')->
            leftjoin('users as commercial', 'commercial.id', '=', 'b.commercial_id')->
            join('users as bou', 'bou.id', '=', 'c.boutiquier_id')->
            join('etats', 'etats.id', '=', 'c.etat_id')->orderBy('date_commande', 'DESC')->paginate(16);

        }

        return view('admin.commandes')->with(['commandes' => $commandes])->with(['categories' => $categoties])
            ->with(['fournisseurs' => $fournisseurs])->with(['commerciaux' => $commerciaux]);
    }

    public function showCommande($commande_id)
    {
        $commande = DB::table('commandes as c')->select('u.nom', 'u.prenom', 'c.date_commande', 'u.phone1', 'c.id', 'etats.libelle as etat')
            ->join('clients as u' , 'u.id', '=', 'c.client_id')
            ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
            ->where('c.id', $commande_id)
            ->get()->first();

        $produits = DB::table('catalogue_commande as cc')->select('p.libelle', 'ca.prix', 'cc.quantite')
            ->join('catalogues as ca', 'ca.id', 'cc.catalogue_id')
            ->join('produits as p', 'p.id', '=', 'ca.produit_id')
            ->where('cc.commande_id', '=', $commande->id )->get();

        $total_commande = 0;
        foreach ($produits as $produit)
        {
            $total  = $produit->prix * $produit->quantite;
            $produit->total = $total;

            $total_commande += $total;

        }
        return view('admin.showCommande')->with(['commande' => $commande])
            ->with(['produits' => $produits])->with(['total_commande' => $total_commande]);
    }

    public function changeStatus(Request $request)
    {
        $statut = $request->get('status');
        $etat_id = Etat::where('libelle', $statut)->get()->first()->id;
        Commande::find((int)$request->get('commande_id'))->update(['etat_id' => $etat_id]);

        return  redirect('admin/commandes');
    }


    public function calling($commande_id)
    {
        Commande::find($commande_id)->update(['call' => 2]);
        return  back();
    }
}
