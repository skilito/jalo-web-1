<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\User;

class AdminClientController extends Controller
{

    public function show($client_id)
    {
        $client = Client::find($client_id);

        return view('admin.client.show')->with(['client' =>$client]);
    }

    public function edit($client_id)
    {

        $user= Client::find($client_id);

        return view('admin.client.edit')->with(['user' => $user]);
    }

    public function  update(Request $request)
    {
        $user_id = (int)$request->get('user_id');

        $user = Client::find($user_id);

        $user->nom = $request->get('editUser_nom');
        $user->prenom = $request->get('editUser_prenom');
        $user->phone1 = $request->get('editUser_tel');
        $user->adresse = $request->get('editUser_adresse');
        $user->save();

        return Redirect::to(route('clients'));
    }

    public function changePassword(Request $request)
    {

        $current_password = \Auth::User()->password;
        if(\Hash::check($request->get('old-password'), $current_password))
        {
            if(($request->get('password') !=  $request->get('password_confirmation')))
            {
                return 'error-confirmation';
            }
            $user_id = \Auth::User()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = \Hash::make($request->get('password'));
            $obj_user->save();
            return 'ok';
        }
        else
        {
            return 'error';
        }

    }
}
