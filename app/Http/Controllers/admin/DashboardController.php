<?php

namespace App\Http\Controllers\Admin;

use App\Boutique;
use App\Catalogue;
use App\Categorie;
use App\Client;
use App\Commande;
use App\Etat;
use App\Http\Controllers\Controller;
use App\Quartier;
use App\User;
use App\Role;
use App\Vente;
use App\Info;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Request;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index()
      {
          $categoties = Categorie::all();

          $array_boutiquier = User::where('role_id', Role::getRoleId('boutiquier'))->orderBy('date_inscription', 'DESC')->get();

          $nbr_boutiquier = $array_boutiquier->count();

          $commande_en_cours = count(DB::table('commandes as c')
              ->join('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')
              ->whereNotNull('c.etat_id')
              ->get());

          $commerciaux = User::where('role_id', Role::getRoleId('commercial'))->orderBy('date_inscription', 'DESC')->get();

          $nbre_commerciaux = $commerciaux->count();

          $produits = Catalogue::all()->count();

          $clients = DB::table('clients')->orderBy('date_creation', 'DESC')->get();

          $nbr_client = $clients->count();

          $fournisseurs = User::where('role_id', Role::getRoleId('fournisseur'))->orderBy('date_inscription', 'DESC');

          $nbr_fournisseur  =$fournisseurs->count();

          $ventes = count(DB::table('commandes as c')
              ->leftjoin('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')
              ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
              ->whereNotNull('c.etat_id')->where('etats.libelle', 'livre')
              ->get());

          $quartiers = Quartier::all();

          return view('admin.dashboard')->with(['nbre_boutiquier' => $nbr_boutiquier])
              ->with(['commade_en_cours' => $commande_en_cours])->with(['commerciaux' => $commerciaux])
              ->with('nbre_commerciaux', $nbre_commerciaux)->with(['array_boutiquier' => $array_boutiquier])
              ->with(['produits' =>$produits])->with(['nbr_client' => $nbr_client])
              ->with(['fournisseurs' => $fournisseurs])->with(['nbr_fournisseur' =>$nbr_fournisseur])
              ->with(['ventes' => $ventes])->with(['clients' => $clients])->with(['quartiers' => $quartiers])
              ->with(['categories' => $categoties]);
      }

    // ajax request
    public function editCommercial(Request $request)
    {
        $commercial_id = $_POST['commercial_id'];

        $user = User::where('id', (int)$commercial_id)->get()->first();

        return response()->json($user);
    }

    public function updateCommercial(Request $request)
    {
        $user = User::where('id', (int)$_POST['commercial_id'])->get()->first();

            $file_name = 'new_photo';
            $file = Request::file($file_name);

            if($file != null) {
                $newFileName = uniqid() . '.' . $file->clientExtension();

                $extension = \File::extension($file_name);

                $file->move(public_path() . '/images/photos', $newFileName);

                $image = 'images/photos/' . $newFileName;
                $user->avatar = $image;
            }

        $user->prenom = $_POST['prenom_commercial'];
        $user->nom = $_POST['nom_commercial'];
        $user->phone1 = $_POST['tel_commercial'];
        $user->adresse = $_POST['adresse_commercial'];
        $user->email = $_POST['email_commercial'];

        $user->save();

        return response()->json('success');
    }

    public function  createBoutiquier()
    {
        $file_name = 'new_photo';
        $file = Request::file($file_name);

        $email = isset($_POST['email_boutiquier']) ? $_POST['email_boutiquier']: "" ;


        if($_POST['password'] != $_POST['confirpassword'])
        {
            return 'errorpassword';
        }

        if ($file != null){
            $newFileName = uniqid() . '.' . $file->clientExtension();

            $extension = \File::extension($file_name);

            $file->move(public_path() . '/images/photos', $newFileName);

            $image = 'images/photos/' . $newFileName;
        }
        else
        {
            $image = null;
        }

        $auth = User::where('phone1', $_POST['tel_boutiquier'])->get()->first();

        if($auth != null)
        {
            return 'error';
        }
        else
        {

            $info_id = Info::where('label', 'internet')->get()->first()->id;

            $role_id = Role::where('role', 'boutiquier')->get()->first()->id;

            $password = bcrypt($_POST['password']);

            $user = User::create(['adresse' => $_POST['adresse_boutiquier'], 'email' => $email,
                'prenom' => $_POST['prenom_boutiquier'], 'nom' => $_POST['nom_boutiquier'], 'phone1' => $_POST['tel_boutiquier'], 'role_id' => $role_id, 'password' => $password,
                'info_id' => $info_id, 'avatar' => $image, 'quartier_id' => $_POST['quartier'], 'date_inscription' => Carbon::now()
            ]);
            #link boutique
            Boutique::create(['boutiquier_id' => $user->id, 'commercial_id' => $_POST['commercial_id']]);
            return response()->json($user);
        }

       return 'ok' ;
    }

    public function editBoutiquier()
    {
        $user = User::where('id', (int)$_POST['boutiquier_id'])->get()->first();
          return response()->json($user);
    }

    public function updateBoutiquier()
    {

        $user = User::where('id', (int)$_POST['boutiquier_id'])->get()->first();

        $file_name = 'new_photo';
        $file = Request::file($file_name);

        if ($file != null){
            $newFileName = uniqid() . '.' . $file->clientExtension();

            $extension = \File::extension($file_name);

            $file->move(public_path() . '/images/photos', $newFileName);

            $image = 'images/photos/' . $newFileName;
            $user->avatar = $image;
        }

        $user->prenom = $_POST['editPrenom_bou'];
        $user->nom = $_POST['editNom_bou'];
        $user->phone1 = $_POST['editTel_bou'];
        $user->adresse = $_POST['editAdresse_bou'];
        $user->email = $_POST['editEmail_bou'];

        $user->save();

        return '';

    }

    public function createClient()
    {
        $info_id = Info::where('label', 'internet')->get()->first()->id;

        $auth = User::where('phone1', $_POST['tel_client'])->get()->first();

        if($_POST['password'] != $_POST['confirpassword'])
        {
            return 'errorpassword';
        }


        if($auth != null)
        {
            return 'error';
        }
        else {
            //dd($_POST);
            if ($_POST['role'] == 'client') {
            $role_id = Role::where('role', 'client')->get()->first()->id;
             }

            if ($_POST['role'] == 'fournisseur') {
                $role_id = Role::where('role', 'fournisseur')->get()->first()->id;
            }

            if ($_POST['role'] == 'commercial') {
                $role_id = Role::where('role', 'commercial')->get()->first()->id;
            }

            $password = bcrypt($_POST['password']);

            $user = User::create(['adresse' => $_POST['adresse_client'], 'email' => $_POST['email_client'],
                'prenom' => $_POST['prenom_client'], 'nom' => $_POST['nom_client'], 'phone1' => $_POST['tel_client'], 'role_id' => $role_id, 'password' => $password,
                'info_id' => $info_id, 'quartier_id' => $_POST['quartier'], 'date_inscription' => Carbon::now()
            ]);

            return 'success';
        }
    }

    public function editClient()
    {

        $user = Client::where('id', (int)$_POST['client_id'])->get()->first();

        return response()->json($user);
    }

    public function updateClient()
    {
        $user = Client::where('id', (int)$_POST['client_id'])->get()->first();

        $user->prenom = $_POST['editPrenom_clien'];
        $user->nom = $_POST['editNom_client'];
        $user->phone1 = $_POST['editTel_client'];
        $user->adresse = $_POST['editAdresse_client'];
        $user->save();

        return '';
    }

    public function addAdmin()
    {
        $auth = User::where('phone1', $_POST['tel_admin'])
                ->orWhere('email', $_POST['EmailAdmin'])
                ->get()->first();

        if($auth != null)
        {
            return 'error';
        }

        $info_id = Info::where('label', 'internet')->get()->first()->id;

        $role_id = Role::where('role', 'admin')->get()->first()->id;

        $password = bcrypt($_POST['PasseAdmin']);

        $quartier_id = Quartier::all()->first()->id;

        $user = User::create(['adresse' => null, 'email' => $_POST['EmailAdmin'],
            'prenom' => $_POST['prenomAdmin'], 'nom' => $_POST['nomAdmin'], 'phone1' => null,'role_id'=> $role_id, 'password' => $password,
            'info_id' => $info_id, 'quartier_id' => $quartier_id, 'date_inscription' => Carbon::now(), 'phone1' => $_POST['tel_admin']
        ]);

       return response()->json($user);

    }

}
