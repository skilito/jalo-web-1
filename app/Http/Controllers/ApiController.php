<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="http://138.197.25.147/api/v1/",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Jalo API",
 *         @SWG\Contact(name="OLale Coumba Sam", url="https://www.google.com"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */

class ApiController extends Controller
{
    //
}
