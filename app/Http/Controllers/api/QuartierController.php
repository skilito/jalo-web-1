<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quartier;

class QuartierController extends Controller
{
    /**
     * @SWG\Get(
     *   path="quartiers",
     *   summary="get all streets",
     *   operationId="getAllQuartier",
     *   tags={"streets"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getAllQuartier()
    {
        $quartiers = Quartier::getQuartier();

        return response()->json(['quartiers' =>$quartiers, 'status' => 200]);
    }

}
