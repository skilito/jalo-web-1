<?php

namespace App\Http\Controllers\Api;

use App\Catalogue;
use App\Client;
use App\Etat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Commande;
use App\CatalogueCommande;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Notification;
use Psy\Command\Command;


class CommandeController extends Controller
{
    /**
     * @SWG\Get(
     *   path="client/commandes",
     *   summary="orders by customer",
     *   operationId="index",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $client_id = JWTAuth::toUser($request->header('Authorization'))->id;

        $commandes = Commande::getCommandes($client_id);

        return response()->json(['commandes_ventes' => $commandes], 200);
    }

    /**
     * @SWG\Post(
     *   path="commande",
     *   summary="create order",
     *   operationId="create",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="command",
     *     in="query",
     *     description="array orders",
     *     required=true,
     *      type="array",
     *      @SWG\Items(
     *             type="integer",
     *             format="int32"
     *         ),
     *         collectionFormat="pipes"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande is created"),
     *   @SWG\Response(response=400, description="catalogue does not exit"),
     *   @SWG\Response(response=504, description="catalogues_id not possed"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public static function create(Request $request)
    {

        $catalogues = $request->get('command');

        $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

        if(isset($catalogues))
        {
            $boutiquier = JWTAuth::toUser($request->header('Authorization'));

            $boutiquier_id = $boutiquier->id;

            $reference = Commande::quickRandom(14);

            $client_id = $request->get('client')["client_id"];

            $client = Client::where('id', $client_id)->first();

            $commande = Commande::create(['date_commande' => Carbon::now(),
                'reference' => $reference,'etat_id' => $etat_id, 'client_id' => $client_id, 'boutiquier_id' => $boutiquier_id, 'call' => false]);


            foreach ($catalogues as $catalogue)
            {
                $commande_id = Commande::where('reference', $commande->reference)->first()->id;

                $catalogue_query = Catalogue::where('id',$catalogue['catalogue_id']);


                if($catalogue_query == [])
                {
                    return response()->json(['message' => 'catalogue does not exit'], 400);
                }

                $catalogueCommande = new CatalogueCommande();

                $catalogueCommande->commande_id = $commande_id;
                $catalogueCommande->catalogue_id = $catalogue['catalogue_id'];
                $catalogueCommande->quantite = $catalogue['quantite'];
                $catalogueCommande->save();

            }
            Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);

            return response()->json(['message' => 'commande is created'], 200);

        }
        else
        {
            return response()->json(['message' =>'catalogues_id not possed'],504);
        }

    }
    /**
     * @SWG\Get(
     *   path="commande/{commande_id}/annulee",
     *   summary="annule order",
     *   operationId="annuleeCommande",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="commande_id",
     *     in="path",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande annuler avec succes"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function annuleeCommande($commande_id)
    {
        if(Commande::annleeCommande($commande_id) == 'success')
        {
            return response()->json(['message' => 'commande annuler avec succes'],200);
        }
        else
        {
            return response()->json(['message' => 'erreur commande non annulee'], 400);
        }
    }

    /**
     * @SWG\Get(
     *   path="boutiquier/{boutiquier_id}/commandes",
     *   summary="orders by shopper",
     *   operationId="getallOrderByShopper",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="operation success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getallOrderByShopper($boutiquier_id)
    {


        $commandes = DB::table('commandes as c')->select('c.date_commande as date',
            'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
            ->join('clients as cl','cl.id', '=', 'c.client_id')
            ->where('c.boutiquier_id', $boutiquier_id)->orderBy('c.date_commande', 'desc')->get();

        if($commandes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($commandes as $commande)
        {
            $produits = DB::table('catalogue_commande as cc')->select('p.libelle as produit', 'cc.quantite', 'ca.prix')
                ->join('catalogues as ca', 'ca.id', '=', 'cc.catalogue_id')
                ->join('produits as p', 'p.id', '=', 'ca.produit_id')
                ->where('cc.commande_id', $commande->id)->get();

            foreach ($produits as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }
            $commande->produits = $produits;

        }

        return response()->json(['commandes' => $commandes], 200);

    }

    public static function calculTotalPrice($quantite, $prix)
    {
        $prixTotal =  $quantite * $prix;

        return $prixTotal;
    }

    /**
     * @SWG\Patch(
     *   path="commande",
     *   summary="update order",
     *   operationId="update",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="commande_id",
     *     in="query",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),

     * @SWG\Parameter(
     *     name="etat",
     *     in="query",
     *     description="status order",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="etat changed success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=504, description="commande_id or etat is empty")
     * )
     *
     */
    public function update(Request $request)
    {
        $commande_id = $request->get('commande_id');

        $etat= $request->get('etat');

        if($commande_id != null && $etat !=null)
        {
            $commande = Commande::find((int)$commande_id);

            if($commande == [])
            {
                return response()->json([message => "ce commmande n'existe pas"]);
            }

            if($etat == "annule")
            {
                $etat_id = Etat::where('libelle', 'annule')->get()->first()->id;
            }
            elseif($etat == "livre")
            {
                $etat_id = Etat::where('libelle', 'livre')->get()->first()->id;
            }
            elseif($etat == "pay_boutiquier")
            {
                $etat_id = Etat::where('libelle', 'pay_boutiquier')->get()->first()->id;
            }
            elseif($etat == "livre_boutique")
            {
                $etat_id = Etat::where('libelle', 'livre_boutique')->get()->first()->id;
                $commande->date_livraison_boutique = Carbon::now();
            }
            elseif($etat == "livre_client")
            {
                $etat_id = Etat::where('libelle', 'livre_client')->get()->first()->id;
                $commande->date_livraison_client = Carbon::now();
            }
            else
            {
                $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;
            }

//            Commande::find($commande_id)->update(['etat_id' => $etat_id]);
              $commande->etat_id = $etat_id;
              $commande->save();

            return response()->json(['message'=> 'etat changed success'],200);

        }
        else
        {
            return response()->json(['message'=> 'commande_id or etat is empty'],404);
        }

    }

    /**
     * @SWG\Get(
     *   path="commercial/commandes",
     *   summary="get orders assigned to commercial",
     *   operationId="getCommandesBycommercial",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="not order")
     * )
     *
     */
    public static function getCommandesBycommercial(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        //dd($user);

        $commandes = Commande::getCommandeByCommercial($user->id);

        if($commandes == [])
        {
            return response()->json(['message' => "not order"],404);
        }

        foreach ($commandes as $commande)
        {
            $produits = DB::table('catalogue_commande as cc')->select('p.libelle as produit', 'cc.quantite', 'ca.prix')
                ->join('catalogues as ca', 'ca.id', '=', 'cc.catalogue_id')
                ->join('produits as p', 'p.id', '=', 'ca.produit_id')
                ->where('cc.commande_id', $commande->id)->get();

            foreach ($produits as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }
            $commande->produits = $produits;

        }

        return response()->json(['commandes' => $commandes], 200);
    }


    public function call($commande_id)
    {
        Commande::find($commande_id)->update(['call' => 1]);
        return response()->json(['message'=> 'commande modifiée avec succés'],200);

    }



}
