<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notification;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    /**
     * @SWG\Get(
     *   path="notification",
     *   summary="send notification",
     *   operationId="create",
     *   tags={"notifications"},
     *   @SWG\Parameter(
     *     name="type",
     *     in="query",
     *     description="type of notification",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=403, description="type is missing")
     * )
     *
     */
    public function create(Request $request)
    {
        $type = $request->get('type');
        if($type)
        {
            if($type == "new_number")
            {
                $object = DB::table('users')->where('role_id', 11)->first();
            }
            $downstreamResponse = Notification::sendNotif($type, $object);
            return response()->json(['reponse' => $downstreamResponse->numberSuccess()], 200);
        }
        else
        {
            return response()->json(['message' => 'type is missing'], 403);
        }

    }
}
