<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use JWTAuth;

class UpdateImageController extends Controller
{
    /**
     * @SWG\Post(
     *   path="user/update/image",
     *   summary="update picture",
     *   operationId="updateImage",
     *   tags={"pictures"},
     *   @SWG\Parameter(
     *         description="file to upload",
     *         in="formData",
     *         name="new_photo",
     *         required=true,
     *         type="file"
     *     ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="update avatar success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     * )
     *
     */
    public function updateImage(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        $file_name = 'new_photo';
        $file = $request->file($file_name );
        $newFileName = uniqid().'.'.$file->clientExtension();

        $extension = \File::extension($file_name);

        $file->move(public_path().'/images/photos', $newFileName);


        $user->avatar ='images/photos/' . $newFileName;

        $user->save();

        return response()->json(['message' => 'update avatar success'], 200);

    }
}
