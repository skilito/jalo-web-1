<?php

namespace App\Http\Controllers\Api;

use App\MaBoutique;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class MaBoutiqueController extends Controller
{
    /**
     * @SWG\Post(
     *   path="add/boutiquier/favoris",
     *   summary="add shopper favorite",
     *   operationId="addBoutiquierFavoris",
     *   tags={"MaBoutique"},
     *   @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="query",
     *     description="id shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="adding succes'"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function  addBoutiquierFavoris(Request $request)
    {

        $user = JWTAuth::toUser($request->header('Authorization'));

        MaBoutique::create(['client_id' => $user->id, 'boutiquier_id' => $request->get('boutiquier_id')]);

        return response()->json(['message' =>'adding succes']);

    }

    /**
     * @SWG\Get(
     *   path="client/boutiquier/favoris",
     *   summary="get list shopper favorite by customer",
     *   operationId="getBoutiquierFavoris",
     *   tags={"MaBoutique"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getBoutiquierFavoris(Request $request)
    {
        $client = JWTAuth::toUser($request->header('Authorization'));

        $role_id_boutiquier = Role::where('role', 'boutiquier')->get()->first()->id;

        $boutiquierFavoris = DB::table('users')
                      ->select('users.nom', 'users.prenom', 'users.avatar', 'users.adresse', 'users.phone1', 'users.id')
                      ->join('ma_boutique as mb', 'mb.boutiquier_id', '=', 'users.id')
                      ->where('mb.client_id', $client->id)->get()->first();

        $autre_boutiquiers = DB::table('users')
                             ->select('users.nom', 'users.prenom', 'users.avatar', 'users.adresse', 'users.phone1', 'users.id')
                             ->where('users.quartier_id', $client->quartier_id)
                             ->where('users.role_id', $role_id_boutiquier)
                             ->where('users.id', '!=' ,$boutiquierFavoris->id)
                             ->get();

        return response()->json(['boutiquier_favoris' => $boutiquierFavoris, 'autre_boutiquier' => $autre_boutiquiers], 200);
    }
}
