<?php

namespace App\Http\Controllers\Api;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Role;
use  App\Quartier;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class UserController extends Controller

{
    /**
     * @SWG\Get(
     *   path="users/fournisseurfmcg",
     *   summary="get all users fournisseurfmcg",
     *   operationId="getUsers",
     *   tags={"users"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getUsers()
    {

        $users = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.date_naissance',
            'u.email','u.sexe','info.label', 'roles.role', 'u.phone1','u.phone2','u.date_inscription')
            ->join('info', 'info.id', '=', 'u.info_id')
            ->join('roles', 'roles.id', '=', 'u.role_id')
            ->where('roles.role', 'fournisseurFMCG')->get();

        return response()->json(['users' => $users, 'status' => 200]);

    }

    /**
     * @SWG\Post(
     *   path="user",
     *   summary="update user",
     *   operationId="update",
     *   tags={"users"},
     *   @SWG\Parameter(
     *     name="nom",
     *     in="query",
     *     description="last name user",
     *     type="string",
     *     required=true
     *   ),
     *  @SWG\Parameter(
     *     name="prenom",
     *     in="query",
     *     description="first name",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="phone1",
     *     in="query",
     *     description="phone1 user",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="quartier_id",
     *     in="query",
     *     description="quartier user",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="stock mis a jour avec succes"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="update not success")
     * )
     *
     */
    public function update(Request $request)
    {


        //$user_id = $request->get('user_id');

        $rules = array(
            'nom'       => 'required|min:2',
            'prenom'     => 'required|min:2',
            'quartier_id'      => 'required',
            'phone1'      => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
         return response()->json(['error' =>$validator->errors(), 'status' =>404],404);
        }

        else
        {

            $user = JWTAuth::toUser($request->header('Authorization'));

            $user->nom = $request->get('nom');
            $user->prenom  = $request->get('prenom');
            $user->adresse  = $request->get('adresse');
            $user->phone1  = $request->get('phone1');
            $user->phone2  = $request->get('phone2');
            $user->sexe = $request->get('sexe');
            $user->quartier_id = $request->get('quartier_id');
            $user->date_naissance  = $request->get('date_naissance');
            $user->email  = $request->get('email');

            if($user->save())
            {
                return response()->json(['message' =>'update success', 'status' => 200]);
            }
            else
            {
                return response()->json(['message' =>'update not success', 'status' => 404],404);
            }
        }
    }

    /**
     * @SWG\Get(
     *   path="roles",
     *   summary="get all users fournisseurfmcg",
     *   operationId="getAllRoles",
     *   tags={"users"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function  getAllRoles()
    {
        $roles = Role::getRoles();

        return response()->json(['roles' =>$roles, 'status' => 200]);
    }

    /**
     * @SWG\Get(
     *   path="boutiquiers",
     *   summary="get all shopper by street",
     *   operationId="getBoutiquiersByQuartier",
     *   tags={"users"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getBoutiquiersByQuartier(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        $boutiquiers = User::getBoutiquiersByQuartier($user);

       return response()->json(['boutiquiers' => $boutiquiers], 200);

    }

    /**
     * @SWG\Get(
     *   path="boutiquier/{boutiquier}/produits",
     *   summary="get all product by shopper",
     *   operationId="getProductByShoper",
     *   tags={"users"},
     * @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getProductByShoper($boutiquier_id)
    {
        return response()->json(['produits' => User::getProduitsByBoutiquier($boutiquier_id)],200);
    }

    public function getFournisseur()
    {
        $fournisseurs = DB::table('users as u')->select('u.nom', 'u.prenom', 'u.phone1', 'u.avatar',
            'u.date_inscription','u.phone2', 'u.sexe','u.slug', 'u.id', 'u.email', 'q.nom', 'i.label', 'r.role')
            ->join('quartiers as q', 'q.id', '=', 'u.quartier_id')
            ->join('info as i', 'i.id', '=', 'u.info_id')
            ->join('roles as r', 'r.id', '=', 'u.role_id')
            ->where('r.role', 'fournisseur')
            ->get();

        return response()->json(['fournisseurs' => $fournisseurs], 200);
    }

}
