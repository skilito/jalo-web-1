<?php

namespace App\Http\Controllers\Api;

use App\Categorie;
use Illuminate\Http\Request;
use App\Produit;
use JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;


class ProduitController extends Controller
{
    /**
     * @SWG\Get(
     *   path="category/products",
     *   summary="get all products by category",
     *   operationId="getListProductToCategory",
     *   tags={"products"},
     *   @SWG\Parameter(
     *     name="category",
     *     in="query",
     *     description="id category",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="this category doesn't exist"),
     * )
     *
     */
    public function getListProductToCategory(Request $request)
    {
        $category = $request->get('category');

        if($category)
        {
            
            if(Categorie::find((int)$category) == null){
                return response()->json(['message' => "this category doesn't exist"],404);
            }

            $produits =  Categorie::where('id', (int)$category)->firstOrFail()->produits()->get();

            return response()->json(['produits' => $produits, 'status' => 200]);
        }
        else
            {
                $produits = Produit::all();
                return response()->json(['produits' => $produits, 'status' => 200]);
        }
    }
    /**
     * @SWG\Get(
     *   path="product/{id}",
     *   summary="show product",
     *   operationId="show",
     *   tags={"products"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id by product",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="tthe product does not exist"),
     * )
     *
     */
    public function show($product)
    {
        if($product)
        {

            if(Produit::find((int)$product) == null)
            {
                return response()->json(['message' => "this product doesn't exist"],404);
            }
            //dd('ok');
            $products = Produit::getProduit((int)$product);

            return response()->json(['product' => $products ,'status' => 200]);
        }
        else
        {
            return response()->json(['message' => 'the product does not exist', 'status' => 404],404);
        }

    }
    /**
     * @SWG\Get(
     *   path="provider/products",
     *   summary="get products by provider",
     *   operationId="getListProductsUser",
     *   tags={"products"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getListProductsUser(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));
        $produits = Produit::getListProduitToUser($user->id);
    
        return response()->json(['produits' => $produits, 'status' => 200]);
    }

    /**
     * @SWG\Get(
     *   path="products",
     *   summary="get all products",
     *   operationId="getAllProduct",
     *   tags={"products"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getAllProduct()
    {
        return response()->json(['produits' => Produit::getAllProduct()], 200);
    }

    /**
     * @SWG\Get(
     *   path="search",
     *   summary="search product",
     *   operationId="search",
     *   tags={"products"},
     * @SWG\Parameter(
     *     name="search",
     *     in="path",
     *     description="item search",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=403, description="search is not exist")
     * )
     *
     */
    public function search(Request $request)
    {
       $search = $request->get('search');

       if($search)
       {

           $catalogues = DB::table('catalogues')
               ->join('users', 'users.id', '=', 'catalogues.fournisseur_id')
               ->join('produits as p', 'p.id', '=', 'catalogues.produit_id')
               ->join('categories as c', 'c.id', '=', 'p.categorie_id')
               ->select('catalogues.*', 'p.libelle as productLibelle',
                   'c.libelle as libelleCategorie', 'users.nom as nomFournisseur','users.prenom as prenomFournisseur','catalogues.image',
                   'users.avatar as avatarFournisseur', 'users.id as fournisseur_id', 'c.icone', 'p.description')
               ->where('p.libelle', 'LIKE', '%' . $search . '%')
               ->orderBy('catalogues.prix', 'DESC')->paginate(8);


           return response()->json(['catalogues' => $catalogues], 200);

       }
       else
       {
           return response()->json(['message' => 'search is not exist'], 403);
       }
    }

}
