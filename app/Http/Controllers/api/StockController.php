<?php

namespace App\Http\Controllers\Api;

use App\Marchandise;
use App\Produit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use JWTAuth;

class StockController extends Controller
{
    /**
     * @SWG\Get(
     *   path="stocks",
     *   summary="get all stocks",
     *   operationId="index",
     *   tags={"stocks"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $userAuth = JWTAuth::toUser($request->header('Authorization'));

        $stock = DB::table('marchandises')
                    ->join('produits', 'produits.id', '=', 'marchandises.produit_id')
                    ->join('categories', 'categories.id', '=', 'produits.categorie_id')
                    ->select('marchandises.*', 'produits.libelle as libelle_produit', 'categories.libelle as categorie')
                    ->where('vendeur_id', $userAuth->id)->get();

        return response()->json(['stock' => $stock], 200);
    }
    /**
     * @SWG\Post(
     *   path="stock",
     *   summary="create stock",
     *   operationId="create",
     *   tags={"stocks"},
     *   @SWG\Parameter(
     *     name="stock",
     *     in="query",
     *     description="array stock",
     *     required=true,
     *      type="array",
     *      @SWG\Items(
     *             type="integer",
     *             format="int32"
     *         ),
     *         collectionFormat="pipes"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="create stock success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function create(Request $request)
    {
        $parameters = $request->get('stock');

        $user_id = JWTAuth::toUser($request->header('Authorization'))->id;

        foreach ($parameters as $parameter)
        {

            $produit = Produit::find((int)$parameter['produit_id']);

            if($produit == null)
            {
                return response()->json(['message' => "this product doesn't exist"]);
            }

            $stock = Marchandise::where('produit_id', (int)$parameter['produit_id'])
                     ->where('vendeur_id', $user_id)->get();

            if($stock == [])
            {
                return response()->json(['message' => "this stock exist"]);
            }

            Marchandise::create(['produit_id' => (int)$parameter['produit_id'], 'vendeur_id' => $user_id,
                'prix' => (int)$parameter['prix'], 'date_ajout' => Carbon::now(), 'quantite_dispo' => (int)$parameter['quantite_dispo']]);

            return response()->json(['message' => 'create stock success']);
        }
    }

    /**
     * @SWG\Patch(
     *   path="stock",
     *   summary="update stock",
     *   operationId="update",
     *   tags={"stocks"},
     *   @SWG\Parameter(
     *     name="marchandise_id",
     *     in="query",
     *     description="id marchandise",
     *     type="integer",
     *     required=true
     *   ),
     *  @SWG\Parameter(
     *     name="quantite_dispo",
     *     in="query",
     *     description="quantity available",
     *     required=false,
     *     type="integer"
     *   ),
     *  @SWG\Parameter(
     *     name="prix",
     *     in="query",
     *     description="price",
     *     required=false,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="stock mis a jour avec succes"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="ce marchandise n'existe pas")
     * )
     *
     */
    public function update(Request $request)
    {
        $marchandise = Marchandise::find((int)$request->get('marchandise_id'));

        if($marchandise == null)
        {
            return response()->json(['message' => "ce marchandise n'existe pas"],404);
        }

        if(($request->get('quantite_dispo')) && $request->get('quantite_dispo') != null)
        {
            $marchandise->quantite_dispo = (int)$request->get('quantite_dispo');
        }

        if(($request->get('prix')) && $request->get('prix') != null)
        {
            $marchandise->prix = (int)$request->get('prix');
        }

        $marchandise->save();

         return response()->json(['message' => 'stock mis a jour avec succes']);
    }
}
