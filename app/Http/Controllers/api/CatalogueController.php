<?php

namespace App\Http\Controllers\Api;

use App\Catalogue;

use Illuminate\Http\Request;
use App\Categorie;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Produit;

class CatalogueController extends Controller
{

    /**
     * @SWG\Get(
     *   path="catalogues/category/{categorie_id}",
     *   summary="get all catalogues by categorie_id",
     *   operationId="index",
     *   tags={"catalogues"},
     *   @SWG\Parameter(
     *     name="categorie_id",
     *     in="path",
     *     description="category by catalogue",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index($categorie_id){

        if($categorie_id == 0 )
        {
            $categorie_id = 'all';
        }
        $catalogues = Catalogue::getAllCatalogue($categorie_id);

        //$product = Produit::all();

        return response()->json(['catalogues' => $catalogues, 'status' => 200]);

    }

    /**
     * @SWG\Get(
     *   path="catalogue/{id}",
     *   summary="show catalogue",
     *   operationId="show",
     *   tags={"catalogues"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id catalogue",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show($catalogue_id){

        if($catalogue_id)
        {
            $catalogue = Catalogue::showcatalogue($catalogue_id);

            return response()->json(['catalogue' => $catalogue ,'status' => 200]);
        }
        else
        {
            return response()->json(['message' => "this catalogue doesn't exist", 'status' => 404],404);
        }
    }

    /**
     * @SWG\Get(
     *   path="categories",
     *   summary="get all categories",
     *   operationId="index",
     *   tags={"categories"},
     *
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getListCataloguePromo()
    {

        $catalogues = Catalogue::getAllCatalogue('promo');

        return response()->json(['cataloguesPromo' => $catalogues, 'status' => 200]);
    }

    /**
     * @SWG\Get(
     *   path="catalogues",
     *   summary="get all catalogues",
     *   operationId="getAllCatalogues",
     *   tags={"catalogues"},
     *
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getAllCatalogues()
    {
        $catalogues = DB::table('catalogues')
            ->join('users', 'users.id', '=', 'catalogues.fournisseur_id')
            ->join('produits as p', 'p.id', '=', 'catalogues.produit_id')
            ->join('categories as c', 'c.id', '=', 'p.categorie_id')
            ->select('catalogues.*', 'p.libelle as productLibelle','c.libelle as libelleCategorie', 'users.nom as nomFournisseur','users.prenom as prenomFournisseur',
                'users.avatar as avatarFournisseur', 'users.id as fournisseur_id','catalogues.image', 'c.icone', 'c.id as categorie_id', 'p.description')
            ->orderBy('catalogues.date_ajout', 'DESC')->get();

        return response()->json(['catalogues' => $catalogues, 'status' => 200]);

    }

    public function incrementNumberView($catalogue)
    {
        $catalogue = Catalogue::find($catalogue);
        if(count($catalogue) >= 1)
        {
            $numberViews = $catalogue->numberViews + 1;
            $catalogue->numberViews = $numberViews;
            $catalogue->save();
            return response()->json(['catalogue' => $catalogue, 'message' => 'numberViews is incremented', 'status' => 200]);

        }
        else
        {
            return response()->json(['message' => "this catalogue doesn't exist", 'status' => 404],404);
        }
    }
}
