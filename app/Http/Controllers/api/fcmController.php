<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\Fcm;
use Carbon\Carbon;

class fcmController extends Controller
{
    /**
     * @SWG\Post(
     *   path="fcm",
     *   summary="create fcm",
     *   operationId="create",
     *   tags={"Fcm"},
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="token adding success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=401, description="token does exist"),
     *   @SWG\Response(response=403, description="token or user is missing")
     * )
     *
     */
    public function create(Request $request)
    {
        $user_id = JWTAuth::toUser($request->header('Authorization'))->id;
        $token = $request->get('token');

        if($user_id && $token)
        {
            $query = Fcm::where('user_id','=', (int)$user_id)->first();

            if($query == null)
            {
                Fcm::create(['user_id' => $user_id,
                    'token' => $token, 'created_at' => Carbon::now()]);
                return response()->json(['message' => 'token adding success'], 200);
            }
            else
            {
                $query->update(['token' => $token]);
                return response()->json(['message'=> 'token is update'],200);
            }
        }
        else
        {
            return response()->json(['message'=> 'token or user is missing'],403);
        }
    }
    /**
     * @SWG\Put(
     *   path="fcm",
     *   summary="update fcm",
     *   operationId="update",
     *   tags={"Fcm"},
     *   @SWG\Parameter(
     *     name="token",
     *     in="query",
     *     description="token",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="token adding success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=401, description="token not exist"),
     *   @SWG\Response(response=403, description="token or user is missing")
     * )
     *
     */
    public function update(Request $request)
    {
        $user_id = JWTAuth::toUser($request->header('Authorization'))->id;
        $token = $request->get('token');

        if($user_id && $token)
        {
            $query = Fcm::where('user_id',(int)$user_id )->first();

            if( $query != null)
            {
                $query->update(['token' => $token]);
                return response()->json(['message'=> 'token is update'],200);
            }
            else
            {
                return response()->json(['message'=> 'token not exist'],401);
            }
        }
        else
        {
            return response()->json(['message'=> 'token or user is missing'],403);
        }

    }
}
