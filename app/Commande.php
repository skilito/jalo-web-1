<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\DB;
use Auth;
use JWTAuth;


class Commande extends Model
{
    use Sluggable;

    protected $table = 'commandes';
    protected $fillable = ['client_id','reference', 'date_commande','slug', 'etat_id', 'boutiquier_id', 'call'];
    public $timestamps = false;

    public static function  getCommandesToUser($categorie)
    {

        if( !isset($categorie))
        {
            $commandes = DB::table('commandes as c')
            ->join('catalogue_commande as cc', 'cc.commande_id', '=' ,'c.id')
            ->join('users', 'users.id', '=', 'c.client_id')
            ->join('catalogues', 'catalogue_id', '=', 'cc.catalogue_id')
            ->join('produits', 'produits.id', '=', 'catalogues.produit_id')
            ->join('categories', 'categories.id', '=', 'produits.categorie_id')
            ->where('c.client_id', Auth::user()->id)
            ->select('produits.libelle as productLibelle',
                'categories.libelle as libelleCategorie', 'users.nom','users.prenom',
                'users.avatar as avatarFournisseur')->paginate(8);
        }
        else
        {
         $commandes = DB::table('catalogue_commande as cc')
            ->join('commandes as c', 'c.id', '=' ,'cc.commande_id')
            ->join('users', 'users.id', '=', 'c.client_id')
            ->join('catalogues', 'catalogue_id', '=', 'cc.catalogue_id')
            ->join('produits', 'produits.id', '=', 'catalogues.produit_id')
            ->join('categories', 'categories.id', '=', 'produits.categorie_id')
            ->where('c.client_id', Auth::user()->id)
            ->where('categories.id', '=', $categorie)
            ->select('produits.libelle as productLibelle',
                'categories.libelle as libelleCategorie', 'users.nom','users.prenom',
                'users.avatar as avatarFournisseur')->paginate(8);
        }
        return $commandes;
    }

    public static function getCommandes($client_id)
    {

        $commandesQuery = DB::table('commandes as c')->select('c.date_commande as date',
                        'c.reference', 'etats.libelle', 'c.id', 'c.call')
                         ->join('etats','etats.id', '=', 'c.etat_id')
                         ->where('c.client_id', $client_id)->orderBy('c.date_commande', 'desc')->get();

        $ventes = DB::table('ventes')->select('ventes.id', 'date_vente as date', 'reference as reference', 'etats.libelle')
            ->join('etats','etats.id', '=', 'ventes.etat_id')->where('ventes.client_id', $client_id)->get();

        foreach ($ventes as $vente)
        {
            $produits = DB::table('marchandise_vente as mv')->select('p.libelle as produit', 'mv.quantite as quantite', 'm.prix')
                ->join('marchandises as m', 'm.id', '=', 'mv.marchandise_id')
                ->join('produits as p', 'p.id', '=', 'm.produit_id')
                ->where('mv.vente_id', $vente->id)->get();
            foreach ($produits as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }

            $vente->produits = $produits;
        }

        foreach ($commandesQuery as $commande)
        {
            $produits = DB::table('catalogue_commande as cc')->select('p.libelle as produit', 'cc.quantite', 'ca.prix')
            ->join('catalogues as ca', 'ca.id', '=', 'cc.catalogue_id')
            ->join('produits as p', 'p.id', '=', 'ca.produit_id')
            ->where('cc.commande_id', $commande->id)->get();

            foreach ($produits as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }
            $commande->produits = $produits;

        }

         return ['commandes' => $commandesQuery, 'ventes' => $ventes];
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'reference'
            ]
        ];
    }

    public static function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }


    public static function calculTotalPrice($quantite, $prix)
    {
         $prixTotal =  $quantite * $prix;

         return $prixTotal;
    }

    public static function annleeCommande($commande_id)
    {
        $etat_annule = Etat::where('libelle', 'annule')->get()->first()->id;

        $commande = self::find($commande_id);

        $commande->etat_id = $etat_annule;

        $commande->save();

        return 'success';
    }

    public static function getCommandeByCommercial($user_id)
    {
        return DB::table('commandes as c')->select('c.id', 'c.date_commande', 'c.reference', 'client.nom as nomClient',
            'client.prenom as prenomClient', 'client.phone1 as phoneClient', 'bou.nom as nomBoutiquier',
            'bou.prenom as prenomBou', 'bou.phone1 as telBou','etats.libelle as etat', 'etats.description as description')->
        join('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')->
        leftjoin('clients as client', 'client.id', '=', 'c.client_id')->
        join('users as bou', 'bou.id', '=', 'c.boutiquier_id')->
        leftjoin('etats', 'etats.id', '=', 'c.etat_id')->where('commercial_id', $user_id)->orderBy('c.date_commande', 'DESC')->get();
    }

}
