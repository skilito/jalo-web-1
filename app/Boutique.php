<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boutique extends Model
{
    protected $table = 'boutiques';

    protected $fillable = ['boutiquier_id', 'commercial_id'];

    public $timestamps = false;
}
