<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenteMarchandise extends Model
{
    protected $table = 'marchandise_vente';
    protected $fillable = ['quantite', 'vente_id','marchandise_id'];
    public $timestamps = false;
}
