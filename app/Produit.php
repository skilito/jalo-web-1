<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\DB;


class Produit extends Model

{
    use Sluggable;
    protected $table = 'produits';
    protected $fillable = ['id','libelle', 'categorie_id', 'description'];
    public $timestamps = false;

    public function categorie()
    {
        return $this->belongsTo(\App\Categorie::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public static function getProduit($product)
    {
        return DB::table('produits')
            ->select('produits.id', 'produits.libelle', 'categories.libelle as category', 'categories.icone', 'p.description')
            ->where('produits.id','=', $product)
            ->join('categories', 'categories.id', '=', 'produits.categorie_id')
            ->get();
    }

    public static function getListProduitToUser($user_id)
    {
        return DB::table('catalogues as c')->select('c.*', 'p.libelle', 'ca.libelle', 'ca.icone', 'p.description')
            ->join('produits as p', 'p.id', '=', 'c.fournisseur_id')
            ->join('categories as ca', 'ca.id', '=','p.categorie_id')
            ->where('c.fournisseur_id', '=', $user_id)
            ->get();
    }

    public static function getAllProduct()
    {
        return DB::table('produits as p')->select('p.id', 'p.libelle as nomProduit', 'c.libelle as categorie', 'c.icone', 'p.description')
            ->join('categories as c', 'c.id', '=','p.categorie_id')
            ->get();

    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'libelle'
            ]
        ];
    }

}
