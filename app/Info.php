<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'info';
    protected $fillable = ['id','label'];
    public $timestamps = false;

    public static function getInfoById($info_id)
    {
    	return self::find($info_id)->label;
    }
}
