<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogueCommande extends Model
{
    protected $table = 'catalogue_commande';
    protected $fillable = ['commande_id', 'catalogue_id', 'quantite'];
    public $timestamps = false;
    
}
